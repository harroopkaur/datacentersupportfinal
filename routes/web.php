<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/en',"HomeController@index");
Route::get('/es',"HomeController@index_es");
Route::get('/EstimadorROI', 'HomeController@EstimadorROI');
Route::get('/calculadoraSPLA', 'HomeController@calculadoraSPLA');
Route::get('/calculadoraSPLASQLServer', 'HomeController@calculadoraSPLASQLServer');
Route::get('/generarDoc', 'HomeController@generarWord2');

Route::get('/maquetado', function(){
	return view('welcome');
});

Route::post('/sendTIPS', 'MailController@sendTIPS');
Route::post('/sendInfoEjecutivo', 'MailController@sendInfoEjecutivo');
Route::get('/maildownloadTIPS', 'MailController@maildownloadTIPS');
Route::get('lang/{locale}', 'HomeController@lang');