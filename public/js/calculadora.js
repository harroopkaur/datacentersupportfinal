
function removeHost(){
    if (count > 1) { $('#bloque' + (count-1)).remove(); count = count - 1;}   
    if (count==2) {
        $('#rHost').attr("disabled", true);
    }else{
        $('#rHost').attr("disabled", false);
    }
    licenciamientoGeneral();
}
function licenciamientoGeneral(){
    var proces = $("input[name='procesadores[]']");
    var coresf = $("input[name='coresFisicos[]']");
    
    var procs = 0;var cores = 0;var vcores = 0;var numLic = 0; var result = 0; var totales=[];
    for (i = 0; i < proces.length; i++) {
        procs = proces[i];
        cores = coresf[i];
        vcores = procs.value * cores.value;
        numLic = Math.ceil(vcores / 2);
        result=licenciamientoWindows(procs.value, vcores, numLic);
        totales.push(result);

        /*console.log(procs.value);
        console.log(cores.value);
        console.log(result);*/
    } 
    var total = 0;
    for (var i = 0; i < totales.length; i++) {
        total += totales[i] << 0;
    }
    $('#totalLicencia').val(parseInt(total));
    //console.log(total);
    
    if($('#ediciones').val() == 1){
        $('#precioUnitario').val(33.39);
    } else{
        $('#precioUnitario').val(4.81);
    }
    
    $('#totalLicenciaRequerida').val(parseInt(total) * parseInt($('#precioUnitario').val()));	
}
function licenciamientoWindows(procs, vcores, numLic){
    if(Math.round(vcores / procs, 0) <= (4 * procs)){
        numLic = 4 * procs;
    }
    return numLic;
}
function onchangeEdiciones(value){
    switch(value) {
        case '1':
            document.getElementById("edicion-selected").innerHTML = "Ent";
            $("#costolicencia").val("600");
            break;
        case '2':
            document.getElementById("edicion-selected").innerHTML = "Std";
            $("#costolicencia").val("140");
            break;
        case '3':
            document.getElementById("edicion-selected").innerHTML = "Web";
            $("#costolicencia").val("17");
            140
            break;
        default:
        // code block
    }
}

function debounce(cb, interval, immediate) {
    var timeout;

    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) cb.apply(context, args);
        };

        var callNow = immediate && !timeout;

        clearTimeout(timeout);
        timeout = setTimeout(later, interval);

        if (callNow) cb.apply(context, args);
    };
};

function keyPressCallback() {
    $("#vmsqldata").html('');
    //var target = document.getElementById('target');
    var input = document.getElementById('vmsql');

    for (var i = 1; i<=input.value; i++){
        $("#vmsqldata").append('<div class="row ptt">' +
            '<div class="col-lg-7 text-right" data-toggle="tooltip" data-placement="Top" title="Segun Datos ingresados arriba">' +
            '<font size="2px" color="#002060"># vCores por VM'+i+'</font>' +
            '</div>' +
            '<div class="col-lg-5 pr-0">' +
            '<input type="number" step="1" min="0" id="vm'+i+'" class="form-control fti" name="coresporvm[]" value="0">' +
            '</div>' +
            '</div>');
    }

    console.log("ENTRA AQUI DEBOUNCE : " + input.value);

}

document.getElementById('vmsql').onkeypress = debounce(keyPressCallback, 400);
function calcularLicenciasSql(cantidadVcores){
    var numLic = calcularLicenciasVcores(cantidadVcores);

    if(parseInt(cantidadVcores) <= 4){
        numLic = 2;
    }
    return numLic;
}
function calcularLicenciasVcores(vcores){
    var numLicen = Math.ceil(vcores / 2);

    return numLicen;
}

function calcularSpla(){
    var cpuPorHost = $("#cpuPorHost").val();
    var coresPorCPU = $("#coresPorCPU").val();
    var vmsql = $("#vmsql").val();
    // MULTIPLICAR CPUs por Host x CORES POR CPU
    var coresovcores1 = cpuPorHost * coresPorCPU;
    $("#coresovcores1").val(coresovcores1);

    var coresporvm = $("input[name='coresporvm[]']");


    var cores= 0;
    var sumCores=0;
    for (var i = 0; i < coresporvm.length; i++) {
        cores = coresporvm[i];
        sumCores += parseInt(cores.value);
        console.log("CORES POR VM : ", cores.value);

    }
    // CALCULAR CORES

    $("#vms1").val(vmsql);
    $("#vms2").val(vmsql);


    $("#coresovcores2").val(sumCores);
    $("#2coresrequeridas1").val(calcularLicenciasSql(coresovcores1));
    $("#2coresrequeridas2").val(calcularLicenciasSql(sumCores));


    $("#costototal1").val(parseInt($("#2coresrequeridas1").val()) * parseInt($("#costolicencia").val()));
    $("#costototal2").val(parseInt($("#2coresrequeridas2").val()) * parseInt($("#costolicencia").val()));
}
function limpiarSpla(){
    $("#cantidadHost").val("0");
    $("#cpuPorHost").val("0");
    $("#coresPorCPU").val("0");
    $("#vmsql").val("0");
    $("#vmsqldata").html("");
    $("#costolicencia").val("0");


    $("#coresovcores1").val("0");
    $("#coresovcores2").val("0");

    $("#vms1").val("0");
    $("#vms2").val("0");

    $("#2coresrequeridas1").val("0");
    $("#2coresrequeridas2").val("0");

    $("#costototal1").val("0");
    $("#costototal2").val("0");
}
(function() {
    keyPressCallback();
})();
