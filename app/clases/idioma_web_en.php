<?php
namespace App\clases;
class idioma_web_en{       
    public $lg0001 = "en";
    
    //inicio navegacion
    public $lg0002 = "Home";
    public $lg0003 = "About Us";
    public $lg0004 = "Services";
    public $lg0005 = "SAM as a Service";
    public $lg0006 = "Audit Defense";
    public $lg0007 = "SAM as a Service for SPLA";
    public $lg0008 = "Deployment Diagnostic";
    public $lg0009 = "Overview";
    public $lg0010 = "Articles";
    public $lg0011 = "Events";
    public $lg0012 = "Colombia";
    public $lg0013 = "Mexico";
    public $lg0014 = "Contact Us";
    public $ln0001 = "Webinar: Conozca cómo administrar el software SPLA mitigando los riesgos de Auditorías";
    public $ln0002 = "Webinar Licenciamiento SAP";
    //fin navegacion
    
    //inicio index
    public $lg0015 = "Software Optimization Made Easy";
    public $lg0016 = "EFFECTIVE AND PERSONALIZED";
    public $lg0017 = "SOFTWARE CONTROL SOLUTIONS";
    public $lg0018 = "circulo 3.png";
    public $lg0019 = "circulo 1.png";
    public $lg0020 = "circulo 2.png";
    public $lg0021 = "gartner.png";
    public $lg0022 = "SAM AS SERVICE";
    public $lg0023 = "The best solution to save by eliminating software";
    public $lg0024 = "overshoots quickly and at low cost";
    public $lg0025 = "best award.png";
    public $lg0026 = "SAM Chantal.png";
    public $lg0027 = "More";
    public $lg0028 = "https://www.youtube.com/embed/-FHh-LuZn2w";
    public $lg0029 = "https://www.youtube.com/embed/kssmN_caomA";
    public $lg0030 = "https://www.youtube.com/embed/rj7D-wgX4NI";
    public $lg0031 = "DISRUPTING THE SAM INDUSTRY";
    public $lg0032 = "LICENSING ASSURANCE";
    public $lg0033 = "CUSTOMERS REVIEWS";
    //fin index
    
   
    
   
    
    //inicio SAM as a Service
    public $lg0071 = "THE COMPLETE SOLUTION";
    public $lg0073 = "gartner.png";
    public $lg0074 = "It is a Multi platform solution of tools, personnel, processes, controls, indicators (KPI) and methodology, 
                    with the purpose of optimizing and controlling the software";
    public $lg0075 = "circulo 2.png";
    public $lg0076 = "CS.png";
    public $lg0077 = "We eliminated the unused software to 0%";
    public $lg0078 = "WE HAVE OUR OWN TOOLS FOCUSED ON SAVINGS";
    public $lg0079 = "LA METERING TOOL";
    public $lg0080 = "A tool is developed to deliver software usability 
                    results like an agent, but not being an agent";
    public $lg0081 = "LA TOOL";
    public $lg0082 = "Proprietary tool for discovery under 
                    Windows environment";
    public $lg0083 = "WEBLOGIC";
    public $lg0084 = "It is a script that allows to identify the 
                    users that connect to this platform as well as to obtain the edition and installed version";
    public $lg0085 = "ALL IN ONE";
    public $lg0086 = "It is a script that runs under Unix that 
                    includes a set of tools that allow you to obtain information from various Oracle products at the 
                    same time";
    public $lg0087 = "LA VTOOL";
    public $lg0088 = "It allows to obtain the virtual environment 
                    infrastructure that is virtualized under VMWare";
    public $lg0089 = "BENEFITS";
    public $lg0090 = "-Greater control for better savings";
    public $lg0091 = "-The best TCO";
    public $lg0092 = "-Multiplatform Service";
    //fin SAM as a Service
    
    //inicio audit defense
   
    
    public $lg0096 = "The expertise of software compliance audit experts to assist you and 
                    protect your company’s assets during a manufacturer software audit";
    public $lg0097 = "Our service is oriented to help you eliminate the audits of your company's software, since having your licensing 
                    system controlled and with 0% unused software, your risk of being called to audit is reduced almost entirely";
    public $lg0098 = "sello3.png";
    public $lg0099 = "BENEFITS";
    public $lg0100 = "Audit mitigation";
    public $lg0101 = "Scenario evaluation";
    public $lg0102 = "Audit Defense Tactics";
    public $lg0103 = "Negotiating with manufacturers to obtain additional added value";
    //fin audit defense
    
    //inicio SPLA
    public $lg0104 = "OPTIMIZE YOUR MONTHLY REPORTS EASY AND FAST";
    public $lg0105 = "SAM AS A SERVICE SPLA";
    public $lg0106 = "We present the best service for SPLA Licensing Reporting. 
                    A multiplatform solution including tools, personnel, processes, controls, KPIs and methodology to 
                    optimize and control software spending.";
    public $lg0107 = "We know:<br>
                    -You are exposed to routine software audits<br>
                    -Your Software usage is continuously over reported<br>
                    -We can increase your profitability with more effective software Controls<br>
                    -You have the right to manage your software without conflict of interest";
    public $lg0108 = "SPLA SERVICE";
    public $lg0109 = "spla chantal.png";
    public $lg0110 = "CONTROL, GENERATES REVENUE AND MITIGATES RISKS";
    //fin SPLA
    
  
    public $lg0114 = "DDWEB.png";
    public $lg0115 = "BENEFITS";
    public $lg0116 = "sello2.png";
    public $lg0117 = "Optimization of: Duplicates, Erroneous, Inactive,Disuse, Virtualization";
    public $lg0118 = "Better and better application control";
    public $lg0119 = "Quick results";
    public $lg0120 = "Easy and effective";
    public $lg0121 = "Better control, ... higher profitability";
    public $lg0122 = "https://www.youtube.com/embed/5rSk2DVqaPw";
    public $lg012523 = "https://www.youtube.com/embed/-FHh-LuZn2w";
    public $lgvid22 = "https://www.youtube.com/embed/6gAjtmw66ak";
    //fin deployment diagnostic
    
    //inicio overview
    public $lg0123 = "oi2.png";
    public $lg0153 = '<map name="map">
        <!-- #$-:Image map file created by GIMP Image Map plug-in -->
        <!-- #$-:GIMP Image Map plug-in by Maurits Rijk -->
        <!-- #$-:Please do not edit lines starting with "#$" -->
        <!-- #$VERSION:2.3 -->
        <!-- #$AUTHOR:Dx -->
        <area shape="rect" coords="237,48,465,239" href="contact.php" target="_blank"/>
        <area shape="rect" coords="477,48,708,241" href="contact.php" target="_blank"/>
        <area shape="rect" coords="716,48,944,240" href="contact.php" target="_blank"/>
        <area shape="rect" coords="958,12,1207,241" href="contact.php" target="_blank"/>
        </map>';
    //fin overview
    
    //inicio articles
    public $lg0124 = "IMPORTANT AND TIMELY INFORMATION";
    public $lg0125 = "ARTICLES LICENSING ASSURANCE";
    public $lg0126 = "https://www.gartner.com/newsroom/id/3382317";
    public $lg0127 = "Gartner Says Organizations Can Cut Software Costs by 30 Percent Using Three Best Practices";
    public $lg0128 = "Many organizations can cut spending on software by as much 
                    as 30 percent by implementing three software license optimization best practices, according 
                    to research from Gartner, Inc. The keys to reducing software license spending are application 
                    configuration optimization, recycling software licenses and by using software asset management 
                    (SAM) tools.";
    public $lg0129 = "http://factorypyme.thestandardit.com/2015/08/17/es-el-software-independiente-una-solucion-para-las-empresas/";
    public $lg0130 = "Is independent software a solution for companies?";
    public $lg0131 = "Licensing Assurance is a pioneer in the software management 
                    market independently, focused on software optimization and licensing. The company states 
                    that in Latin America, it is important to have independent advice for making decisions when 
                    purchasing software.";
    public $lg0132 = "http://samforyoula.blogspot.com/";
    public $lg0133 = "5 errors to avoid during the execution of Software Asset Management processes";
    public $lg0134 = "The SAM works according to four steps or phases: first 
                    an analysis of the facilities is carried out in order to make a diagnosis process of their status. 
                    Then the execution of the licensing is determined, identifying faults and rights. In the next step 
                    a report is prepared of everything investigated and finally proceeds to carry out the negotiation, 
                    that is, the purchase of licenses. For you to have more information on this topic, we will show you 
                    a brief selection of 5 common mistakes that should be avoided when running SAM processes, so that 
                    they can be more profitable.";
    public $lg0135 = "We have many more things to say, ... Visit our Blog";
    public $lg0136 = "Let's talk";
    public $lg0155 = "";
    public $lg0156 = "";
    public $lg0157 = "";
    //fin articles
    
   
    
    //inicio redireccionamiento navegacion
    public $nv0001 = "index.php";
    public $nv0002 = "aboutUs";
    public $nv0003 = "services";
    public $nv0004 = "SAMasaService";
    public $nv0005 = "auditDefense";
    public $nv0006 = "SPLA";
    public $nv0007 = "deployment";
    public $nv0008 = "overview";
    public $nv0009 = "articles";
    public $nv0010 = "eventsColombia";
    public $nv0011 = "eventsMexico";
    public $nv0012 = "contact";
    public $nv0013 = "webinar";
    public $nv0014 = "webinar_licenciamiento_SAP";
    public $nvs0015 = "datacenters";
    public $nvs0016 = "blog";
    public $nvs0017 = "webinar";
    public $nvs0018 = "webcast2";
    public $nvs0019 = "toolsapps";
    public $nvs0020 = "news";
    public $nvs0021 = "sitemap";
    public $nvs0022 = "blogdetail";
    //fin redireccionamiento navegacion
    
    //inicio foot
    public $ft0001 = "OUR PRODUCTS";
    public $ft0002 = "OUR TEAM";
    public $ft0003 = "index.php?videos=true";
    public $ft0004 = "CONTACT US!!";
    public $ft0005 = "Phone: (305) 851-3545";
    public $ft0006 = "info@licensingassurance.com";
    public $ft0007 = "Licensing Assurance LLC";
    public $ft0008 = "16192 Coastal Highway";
    public $ft0009 = "Lewes, DE 19958";
    public $ft0010 = "We are available";
    public $ft0011 = "Monday - Friday";
    public $ft0012 = "8:00 am - 5:00 pm";
    public $ft0013 = "East Time";
    public $ft0014 = "Copyright Licensing Assurance LLC. All Rights Reserved";
    public $ft0015 = "Follow us and Share";
    //fin foot


 

 

  

   

}
