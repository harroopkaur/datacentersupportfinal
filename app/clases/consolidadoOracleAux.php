<?php

namespace App\Clases;

class consolidadoOracleAux extends General{
    public  $lista        = array();
    public  $listaEdicion = array();
    public  $listaVersion = array();
    public  $edicion      = "";
    public  $version      = "";
    public  $error        = NULL;
    
    // Insertar 
    function insertar($cliente, $empleado, $dato_control, $host_name, $registro, $editor, $version, $fecha_instalacion, $software) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidado_oracleAux (cliente, empleado, host_name, product, type, description, file) VALUES '
            . '(:cliente, :empleado, :host_name, :product, :type, :description, :file)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':host_name'=>$host_name, ':product'=>$product, ':type'=>$type, ':description'=>$description, ':file'=>$file));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
  
    // Eliminar
    function eliminar($cliente, $empleado) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM consolidado_oracleAux WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarProductos($cliente, $empleado, $software) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM consolidado_oracleAux
            WHERE cliente = :cliente AND empleado = :empleado AND product LIKE :product
            GROUP BY cliente, empleado, host_name, product
            ORDER BY host_name DESC');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':product'=>"%" . $software . "%"));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}