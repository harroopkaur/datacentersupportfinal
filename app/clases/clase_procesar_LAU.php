<?php

namespace App\Clases;

class clase_procesar_LAU extends General
{
    private $client_id;
    private $client_empleado;
    private $idDiagnostic;
    private $fecha;
    private $baseLAU;
    private $archivo;
    private $tabLAU;
    private $tabLAUAux;
    private $campoLAU;
    private $campoLAU1;
    private $arrayLAU;
    private $bloqueValoresLAU1;
    private $opcionDespliegue;
    private $opcion;
    private $procesarLAU;

    private $iDN;
    private $iobjectClass;
    private $icn;
    private $iuserAccountControl;
    private $ilastLogon;
    private $ipwdLastSet;
    private $isAMAccountName;
    private $ilastLogonTimestamp;
    private $imail;
    private $DN;
    private $objectClass;
    private $cn;
    private $userAccountControl;
    private $lastLogon;
    private $pwdLastSet;
    private $sistema;
    private $saMAccountName;
    private $lastLogonTimestamp;
    private $Mail;
    private $minimor;
    private $dias1;
    private $dias2;
    private $dias3;
    private $fechaDespliegue;

    private $tabDiagnosticos;

    public function insertarEnBloque($bloque, $bloqueValores)
    {
        try {
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO " . $this->tabLAU . " (" . $this->campoLAU . ", dn, objectclass,
            cn,  lastlogon, pwdlastset, sAMAccountName, lastLogonTimestamp, mail, minimo)
            VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    public function insertarEnBloqueAux($bloque, $bloqueValores)
    {
        try {
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO " . $this->tabLAUAux . " (" . $this->campoLAU . ", dn, objectclass,
            cn, lastlogon, pwdlastset, sAMAccountName, lastLogonTimestamp, mail, minimo)
            VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // public function insertar($cliente, $idCorreo, $cn, $os)
    // {
    //     try {
    //         $this->conexion();
    //         $sql = $this->conn->prepare("INSERT INTO appFilepcs (cliente, idCorreo, dn, objectclass,
    //         cn, useracountcontrol, lastlogon, pwdlastset, os, lastLogonTimestamp)
    //         VALUES (:cliente, :idCorreo, NULL, NULL, :cn, NULL, NULL, NULL, :os, NULL)");
    //         $sql->execute(array(":cliente" => $cliente, ":idCorreo" => $idCorreo, ":cn" => $cn, ":os" => $os));
    //         return true;
    //     } catch (PDOException $e) {
    //         echo $this->error = $e->getMessage();
    //         return false;
    //     }
    // }

    public function actualizarEquipo()
    {
        $this->conexion();
        $query = "UPDATE " . $this->tabLAU . " SET dn = :dn, objectclass = :objectclass,
        lastlogon = :lastlogon, pwdlastset = :pwdlastset, sAMAccountName = :sAMAccountName, lastLogonTimestamp = :lastLogonTimestamp, mail = :mail WHERE cliente = :cliente
        AND cn = :cn";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente' => $this->client_id, ':dn' => $this->dn, ':objectclass' => $this->objectclass, ':cn' => $this->cn, ':lastlogon' => $this->lastlogon, ':pwdlastset' => $this->pwdlastset, ':sAMAccountName' => $this->sAMAccountName, ':lastLogonTimestamp' => $this->lastLogonTimestamp, ':mail' => $this->mail));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    public function eliminar()
    {
        $this->conexion();
        $query = "DELETE FROM " . $this->tabLAU . " WHERE cliente = :cliente";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente' => $this->client_id));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    public function eliminarAux()
    {
        $this->conexion();
        $query = "DELETE FROM " . $this->tabLAUAux . " WHERE cliente = :cliente";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente' => $this->client_id));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    // public function eliminarAppEscaneo($cliente, $idCorreo)
    // {
    //     $this->conexion();
    //     $query = "DELETE FROM appFilepcs WHERE cliente = :cliente AND idCorreo = :idCorreo";
    //     try {
    //         $this->conexion();
    //         $sql = $this->conn->prepare($query);
    //         $sql->execute(array(':cliente' => $cliente, ":idCorreo" => $idCorreo));
    //         return true;
    //     } catch (PDOException $e) {
    //         $this->error = $e->getMessage(); //"No se pudo agregar el registro";
    //         return false;
    //     }
    // }

    public function actualizarLAU($id, $archivo)
    {
        $query = "UPDATE " . $this->tabDiagnosticos . " SET LAU = :archivo WHERE id = :id";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id' => $id, ':archivo' => $archivo));
            return true;
        } catch (PDOException $e) {
            echo $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    public function codigo_existe($codigo, $id)
    {
        $query = "SELECT * FROM tabla_conversion WHERE codigo = :codigo AND id != :id";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':codigo' => $codigo, ':id' => $id));
            $row = $sql->fetch();
            if ($sql->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    public function datos2($codigo)
    {
        $query = "SELECT * FROM tabla_conversion WHERE codigo = :codigo";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':codigo' => $codigo));
            $usuario = $sql->fetch();

            return $usuario['os'];
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return array("os" => "");
        }
    }

    public function obtenerSistema($sistema)
    {
        if ($this->codigo_existe($sistema, 0)) {
            $sistema = $this->datos2($sistema);
        }

        return $sistema;
    }

    public function existeEquipo()
    {
        $this->conexion();
        $query = "SELECT COUNT(cn) AS cantidad "
        . "FROM " . $this->tabLAU . " "
        . "WHERE " . $this->campoLAU1 . " AND cn = :cn";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute($this->bloqueValoresLAU1);
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return 0;
        }
    }

    public function listadoEquiposDuplicados()
    {
        try {
            $this->conexion();
            $query = "SELECT MIN(id) AS id
                FROM " . $this->tabLAU . "
                WHERE " . $this->campoLAU1 . "
                GROUP BY cn
                HAVING COUNT(cn) > 1";
            $sql = $this->conn->prepare($query);
            $sql->execute($this->arrayLAU);
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return array();
        }
    }

    public function eliminarEquiposDespliegue()
    {
        try {
            $tabla = $this->listadoEquiposDuplicados();
            while (count($tabla) > 0) {
                $elimId = $this->stringConsulta($tabla, "id");
                $query  = "DELETE FROM " . $this->tabLAU . " WHERE " . $this->campoLAU1 . " AND id IN (" . $elimId . ")";

                $sql = $this->conn->prepare($query);
                $sql->execute($this->arrayLAU);
                $tabla = $this->listadoEquiposDuplicados();
            }

            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    public function setTabConsolidado($tabLAU)
    {
        $this->tabLAU = $tabLAU;
    }

    public function setTabConsolidadoAux($tabLAUAux)
    {
        $this->tabLAUAux = $tabLAUAux;
    }

    public function setTabDiagnosticos($opcion)
    {
        if ($opcion == "Cloud") {
            $this->tabDiagnosticos = "MSCloud";
        } else if ($opcion == "Diagnostic") {
            $this->tabDiagnosticos = "SAMDiagnostic";
        }
    }

    public function moverArchivoLAU($client_id, $client_empleado, $temp, $incremArchivo, $opcionDespliegue, $opcion, $idDiagnostic = 0)
    {
        $this->client_id        = $client_id;
        $this->client_empleado  = $client_empleado;
        $this->idDiagnostic     = $idDiagnostic;
        $this->fecha            = date("dmYHis");
        $this->opcionDespliegue = $opcionDespliegue;
        $this->opcion           = $opcion;

        $this->cabeceraTablas();

        $this->archivo = "";
        if ($this->client_id > 0 && $this->client_empleado > 0) {
            $this->baseLAU = storage_path('app/files/' . $this->opcion . '/archivos_csvf7/');

            $this->archivo .= $this->client_id . $this->client_empleado . "_";
        } else {
            $this->baseLAU = storage_path('app/files/archivosLAU');
        }
        $this->archivo .= "LAU_Output" . $this->fecha . ".csv";

        if ($this->opcionDespliegue == "completo" || $this->opcionDespliegue == "segmentado") {
            move_uploaded_file($temp, $this->baseLAU . $this->archivo);
            $this->eliminar();
        } else {
            $this->baseLAU .= "incremento/";

            if (!file_exists($this->baseLAU)) {
                mkdir($this->baseLAU, 0755, true);
            }

            move_uploaded_file($temp, $this->baseLAU . $this->archivo);
            if ($incremArchivo == 0) {
                $this->eliminar();
            }
            $this->eliminarAux();
        }

        if ($this->opcion == "Cloud" || $this->opcion == "Diagnostic") {
            $this->setTabDiagnosticos($this->opcion);
            $this->actualizarLAU($idDiagnostic, $this->archivo);
        }
    }

    public function procesarLAU_users($iDN, $iobjectClass, $icn, $ilastLogon, $ipwdLastSet, $isAMAccountName, $ilastLogonTimestamptamp, $imail, $procesarLAU, $fechaDespliegue, $opcionDespliegue)
    {
        $this->iDN                     = $iDN;
        $this->iobjectClass            = $iobjectClass;
        $this->icn                     = $icn;
        $this->ilastLogon              = $ilastLogon;
        $this->ipwdLastSet             = $ipwdLastSet;
        $this->isAMAccountName         = $isAMAccountName;
        $this->ilastLogonTimestamptamp = $ilastLogonTimestamptamp;
        $this->imail                   = $imail;
        $this->procesarLAU             = $procesarLAU;       
        $this->opcionDespliegue        = $opcionDespliegue;

         $this->fechaDespliegue = strtotime($fechaDespliegue);

        if ($this->obtenerSeparadorUniversal($this->baseLAU . $this->archivo, 2, 8) === true && $this->procesarLAU === true) {
            if (($fichero = fopen($this->baseLAU . $this->archivo, "r")) !== false) {
                $this->cicloInsertar($fichero);

                $this->verificarRegistrosInsertar1();

                fclose($fichero);

                $this->eliminarEquiposDespliegue();
            }
        }
    }

    public function procesarLAUAppEscaneo($archivo, $procesarSO, $cliente, $idCorreo)
    {
        if ($this->obtenerSeparadorUniversal($archivo, 2, 4) === true && $procesarSO === true) {
            if (($fichero = fopen($archivo, "r")) !== false) {
                $i = 1;
                while (($datos = fgetcsv($fichero, 1000, $this->separador)) !== false) {
                    if ($i > 2) {
                        $cn = "";
                        if (isset($datos[1])) {
                            $cn = $this->truncarString(utf8_encode($datos[1]), 250);
                        }

                        $this->insertar($cliente, $idCorreo, $cn, $datos[2]);
                    }
                    $i++;
                }

                fclose($fichero);
            }
        }
    }

    public function cabeceraTablas()
    {
        if ($this->opcion == "Cloud") {
            $this->tabLAU            = "filepcsMSCloud";
            $this->campoLAU          = "idDiagnostic";
            $this->campoLAU1         = "idDiagnostic = :idDiagnostic";
            $this->bloqueValoresLAU1 = array(":idDiagnostic" => $this->idDiagnostic, ":cn" => $this->cn);
            $this->arrayLAU          = array(":idDiagnostic" => $this->idDiagnostic);
        } else if ($this->opcion == "Diagnostic") {
            $this->tabLAU            = "filepcsSAMDiagnostic";
            $this->campoLAU          = "idDiagnostic";
            $this->campoLAU1         = "idDiagnostic = :idDiagnostic";
            $this->bloqueValoresLAU1 = array(":idDiagnostic" => $this->idDiagnostic, ":cn" => $this->cn);
            $this->arrayLAU          = array(":idDiagnostic" => $this->idDiagnostic);
        } else if ($this->opcion == "microsoft") {
            $this->tabLAU            = "lau_users";
            $this->tabLAUAux         = "lau_usersaux";
            $this->campoLAU          = "cliente, empleado";
            $this->campoLAU1         = "cliente = :cliente";
            $this->bloqueValoresLAU1 = array(":cliente" => $this->client_id, ":cn" => $this->cn);
            $this->arrayLAU          = array(":cliente" => $this->client_id);
        } else if ($this->opcion == "appEscaneo") {
            $this->tabLAU            = "appFilepcs";
            $this->campoLAU          = "idDiagnostic, idCorreo";
            $this->campoLAU1         = "idDiagnostic = :idDiagnostic";
            $this->bloqueValoresLAU1 = array(":idDiagnostic" => $this->idDiagnostic, ":cn" => $this->cn);
            $this->arrayLAU          = array(":idDiagnostic" => $this->idDiagnostic);
        } else if ($this->opcion == "oracle") {
            $this->tabLAU            = "filepcs_oracle";
            $this->tabLAUAux         = "filepcs_oracleAux";
            $this->campoLAU          = "cliente, empleado";
            $this->campoLAU1         = "cliente = :cliente";
            $this->bloqueValoresLAU1 = array(":cliente" => $this->client_id, ":cn" => $this->cn);
            $this->arrayLAU          = array(":cliente" => $this->client_id);
        }

    }

    public function cicloInsertar($fichero)
    {
        $i                    = 1;
        $j                    = 0;
        $this->bloque         = "";
        $this->bloqueValores  = array();
        $this->insertarBloque = false;

        while (($datos = fgetcsv($fichero, 1000, $this->separador)) !== false) {
            $this->cn = "";
            if (isset($datos[$this->icn])) {
                $this->cn = $this->truncarString(utf8_encode($datos[$this->icn]), 250);
            }

            $existeEquipo = $this->existeEquipo();


            if ($i > 1 && $existeEquipo == 0) {
                $this->crearBloque($j);

                $this->setValores($datos);
                $this->crearBloqueValores($j);

                $j = $this->verificarRegistrosInsertar($j);

                $j++;
            } else if ($i > 1 && $existeEquipo > 0) {
                $this->setValores($datos);
                $this->actualizarEquipo();
            }

            $i++;
        }
    }

    public function crearBloque($j)
    {
        $inicioBloque = "(:cliente" . $j . ", :empleado" . $j . ", ";
        if ($this->opcion == "Cloud" || $this->opcion == "Diagnostic") {
            $inicioBloque = "(:idDiagnostic" . $j . ", ";
        } else if ($this->opcion == "appEscaneo") {
            $inicioBloque = "(:idDiagnostic" . $j . ", idCorreo" . $j . ", ";
        }

        if ($j == 0) {
            $this->insertarBloque = true;
            $this->bloque .= $inicioBloque . ":dn" . $j . ", :objectclass" . $j . ", "
                . ":cn" . $j . ", :lastlogon" . $j . ", :pwdlastset" . $j . ", "
                . ":sAMAccountName" . $j . ", :lastLogonTimestamp" . $j . ", :mail" . $j . ", :minimo" . $j. ")";
        } else {
            $this->bloque .= ", " . $inicioBloque . ":dn" . $j . ", :objectclass" . $j . ", "
                . ":cn" . $j . ", :lastlogon" . $j . ", :pwdlastset" . $j . ", "
                . ":sAMAccountName" . $j . ", :lastLogonTimestamp" . $j . ", :mail" . $j . ", :minimo" . $j. ")";
        }
    }

    public function crearBloqueValores($j)
    {
        if ($this->opcion == "Cloud" || $this->opcion == "Diagnostic") {
            $this->bloqueValores[":idDiagnostic" . $j] = $this->idDiagnostic;
        } else if ($this->opcion == "appEscaneo") {
            $this->bloqueValores[":idDiagnostic" . $j] = $this->idDiagnostic;
            $this->bloqueValores[":idCorreo" . $j]     = $this->idCorreo;
        } else {
            $this->bloqueValores[":cliente" . $j]  = $this->client_id;
            $this->bloqueValores[":empleado" . $j] = $this->client_empleado;
        }

        $this->bloqueValores[":dn" . $j]                 = $this->DN;
        $this->bloqueValores[":objectclass" . $j]        = $this->objectClass;
        $this->bloqueValores[":cn" . $j]                 = $this->cn;
        $this->bloqueValores[":lastlogon" . $j]          = $this->lastLogon;
        $this->bloqueValores[":pwdlastset" . $j]         = $this->pwdLastSet;
        $this->bloqueValores[":sAMAccountName" . $j]     = $this->saMAccountName;
        $this->bloqueValores[":lastLogonTimestamp" . $j] = $this->lastLogonTimestamptamp;
        $this->bloqueValores[":mail" . $j]               = $this->Mail;
        $this->bloqueValores[":minimo" . $j]             = $this->minimor;
    }

    public function verificarRegistrosInsertar($j)
    {
        if ($j == $this->registrosBloque) {
            $this->insertarGeneral();

            $this->bloque         = "";
            $this->bloqueValores  = array();
            $j                    = -1;
            $this->insertarBLoque = false;
        }

        return $j;
    }

    public function insertarGeneral()
    {
        if (!$this->insertarEnBloque($this->bloque, $this->bloqueValores)) {
            //echo $this->error;
        }

        if ($this->opcionDespliegue != "completo" && $this->opcionDespliegue != "segmentado") {
            if (!$this->insertarEnBloqueAux($this->bloque, $this->bloqueValores)) {
                //echo $this->error;
            }
        }
    }

    public function verificarRegistrosInsertar1()
    {
        if ($this->insertarBloque === true) {
            $this->insertarGeneral();
        }
    }

    public function setValores($datos)
    {
        $this->DN = "";
        if (isset($datos[$this->iDN])) {
            $this->DN = $this->truncarString($this->get_escape(utf8_encode($datos[$this->iDN])), 250);
        }

        $this->objectClass = "";
        if (isset($datos[$this->iobjectClass])) {
            $this->objectClass = $this->truncarString($this->get_escape(utf8_encode($datos[$this->iobjectClass])), 250);
        }

        $this->lastLogon = "";
        if (isset($datos[$this->ilastLogon])) {
            $this->lastLogon = $this->truncarString($this->get_escape(utf8_encode($datos[$this->ilastLogon])), 250);
        }

        $this->pwdLastSet = "";
        if (isset($datos[$this->ipwdLastSet])) {
            $this->pwdLastSet = $this->truncarString($this->get_escape(utf8_encode($datos[$this->ipwdLastSet])), 250);
        }

        $this->saMAccountName = "";
        if (isset($datos[$this->isAMAccountName])) {
            $this->saMAccountName = $this->obtenerSistema($datos[$this->isAMAccountName]);
        }

        $this->lastLogonTimestamptamp = "";
        if (isset($datos[$this->ilastLogonTimestamptamp])) {
            $this->lastLogonTimestamptamp = $this->truncarString($this->get_escape(utf8_encode($datos[$this->ilastLogonTimestamptamp])), 250);
        }

        $this->Mail = "";
        if (isset($datos[$this->imail])) {
            $this->Mail = $this->truncarString($this->get_escape(utf8_encode($datos[$this->imail])), 250);
        }

        $this->dias1 = $this->obtenerDias($this->lastLogon);
        $this->dias2 = $this->obtenerDias($this->pwdLastSet);
        $this->dias3 = $this->obtenerDias($this->lastLogonTimestamptamp);
        $this->obtenerMinimo();
    }

    public function obtenerDias($fecha)
    {        
        $dias = null;

        if (is_numeric($fecha)) {
            $value = round(($fecha - 116444735995904000) / 10000000);
            $dias  = $this->daysDiff($value, $this->fechaDespliegue);
        }

        return $dias;
    }

    public function obtenerMinimo()
    {
        $minimos       = $this->minimo($this->dias1, $this->dias2, $this->dias3);
        $this->minimor = round(abs($minimos), 0);

        // if ($this->minimor <= 30) {
        //     $this->minimo = 1;
        // } else if ($this->minimor <= 60) {
        //     $this->minimo = 2;
        // } else if ($this->minimor <= 90) {
        //     $this->minimo = 3;
        // } else if ($this->minimor <= 365) {
        //     $this->minimo = 4;
        // } else {
        //     $this->minimo = 5;
        // }

        // if ($this->minimo < 4) {
        //     $this->activo = 1;
        // } else {
        //     $this->activo = 0;
        // }
    }
}
