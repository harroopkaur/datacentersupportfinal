<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;

class HomeController extends Controller {


     public function lang($locale)
    {
        App::setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back();
    }

    public function EstimadorROI(){

		return view('estimadorR');
	}

	public function calculadoraSPLA(){

		return view('calculadoraSPLA');
	}
	public function calculadoraSPLASQLServer(){
        return view('calculadoraSPLASQLServer');
    }
    public function generarWord(){
        
$phpWord = new \PhpOffice\PhpWord\PhpWord();
$phpWord->addParagraphStyle('Heading2', array('alignment' => 'center'));
$section = $phpWord->addSection();


$textbox = $section->addTextBox(
    array(
        'alignment'   => \PhpOffice\PhpWord\SimpleType\Jc::RIGHT,
        'width'       => 170,
        'height'      => 100,
        'borderSize'  => 0,
        'borderColor' => '#FFF',
        'size' => 17
    )
);
$textbox->addText('___ de ______, 20__',array('bold'=>true));
$textbox->addText('Licensing Assurance LLC',array('bold'=>true));
$textbox->addText('16192 Coastal Highway');
$textbox->addText('Lewes, DE 19958');
$textbox->addText('Teléfono USA: (305) 851-3545');
$textbox->addText('Teléfono USA: (305) 767-4018');
$textbox->addText('www.LicensingAssurance.com',array('bold'=>true));

$section->addTextBreak(1);
$section->addTextBreak(1);
$section->addTextBreak(1);

$section->addImage(public_path('/images/documentRoiCenter.png'), array(
    'width'       => 400,
    'height'      => 100,
    'alignment' => 'center'
));
$section->addTextBreak(1);
$section->addShape(
    'rect',
    array(
        'roundness' => 0.2,
        'frame'     => array('width' => 400, 'height' => 2, 'left' => 0, 'top' => 1),
        'fill'      => array('color' => '#00b0f0'),
        'outline'   => array('color' => '#00b0f0', 'weight' => 1),
        'shadow'    => array(),
    )
);

$section->addText('SAM as a Service DC – Business Case',
array(  
    'alignment' => 'center',
    'color' => '059bd2',
    'size' => 24
)
);
$section->addShape(
    'rect',
    array(
        'roundness' => 0.2,
        'frame'     => array('width' => 400, 'height' => 2, 'left' => 0, 'top' => 1),
        'fill'      => array('color' => '#00b0f0'),
        'outline'   => array('color' => '#00b0f0', 'weight' => 1),
        'shadow'    => array(),
    )
);



$footer = $section->addFooter();
$footer->firstPage();
$footer->addText('SAM as a Service DC aumenta la rentabilidad reduciendo el costo de la administración de software, optimizando los reportes y mitigando el riesgo de auditoría a través del control efectivo del software. ', 
array(
    'alignment' => 'left',
    'size' => 14
));





/*
$header = $section->addHeader();
        $header->firstPage();
        $header->addImage(public_path('/images/documentRoiHeader.png'),array('width' => 450,
         'height' => 100,
        'positioning' => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
        'posHorizontal' => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_LEFT,
        'posVertical' => \PhpOffice\PhpWord\Style\Image::POSITION_VERTICAL_TOP,
        'marginLeft'       => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(10),
        'marginTop'        => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(8),
    ));

*/

// Save file
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save(storage_path('documentos_generados/Appdividend.docx'));
        return response()->download(storage_path('documentos_generados/Appdividend.docx'));

    }
	public function generarWord2(){
        
		// $phpWord = new PhpWord();
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/wordtemplate/LA - Portal -Business Case- ROI.docx'));
        $templateProcessor->setValue('dia', '15 ');
        $templateProcessor->setValue('mes', 'Marzo');
        $templateProcessor->setValue('ano', '19');
        // $templateProcessor->setValue(array('City', 'Street'), array('Detroit', '12th Street'));
        // $section = $phpWord->addSection();

        // $header = $section->addHeader();
        // $header->firstPage();
        // $header->addImage(public_path('/images/documentRoiHeader.png'),array('width' => 80, 'height' => 80));


        // // $text = $section->addText('nombre');
        // // $text = $section->addText('email');
        // // $text = $section->addText('number',array('name'=>'Arial','size' => 20,'bold' => true));
        // //$section->addImage(public_path('/images/documentRoiHeader.png'));  
        // //$section->addImage(public_path('/images/documentRoiCenter.png'));  
        // $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $time = time();
        $templateProcessor->saveAs(storage_path('documentos_generados/'.$time.'.docx'));
        return response()->download(storage_path('documentos_generados/'.$time.'.docx'));
	}
}