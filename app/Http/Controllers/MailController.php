<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\FlashServiceProvider;
use Illuminate\Support\Facades\Mail;
use App\Mail\AuditoriaTIPS;
use App\Mail\InfoEjecutivo;

class MailController extends Controller
{
    public function sendTIPS(Request $request){

    	if(!isset($request['name']) || $request['name'] == ''){
    		flash('El nombre no puede estar vacio', 'danger');
    		return redirect('/');
    	}
    	if(!isset($request['pais']) || $request['pais'] == ''){
    		flash('El pais no puede estar vacio', 'danger');
    		return redirect('/');
    	}
    	
    	if(!isset($request['email']) || $request['email'] == ''){
    		flash('El email no puede estar vacio', 'danger');
    		return redirect('/');
    	}elseif(!filter_var($request['email'], FILTER_VALIDATE_EMAIL)){
    		flash('Formato de email incorrecto', 'danger');
    		return redirect('/');
    	}

    	Mail::to($request['email'],'Licensing Assurance DC Solutions')->send(new AuditoriaTIPS($request['name']));

	    if( count(Mail::failures()) > 0 ) {
		   	$error = "Hubo uno o varios errores. Estos fueron: <br>";
		   	foreach(Mail::failures as $email_address) {
		       	$error += " - ".$email_address." <br>";
		    }
	    	flash($error, 'danger');
		}else{
	    	flash('Se ha enviado el mensaje de manera exitosa', 'success');
		}
    	return redirect('/');
    }

    public function maildownloadTIPS(){
    	return response()->download(storage_path('files/LA-Audit_Support_SPLA.pdf'));
    }

    public function sendInfoEjecutivo(Request $request){

        // dd($request['fab']);

        if(!isset($request['name']) || $request['name'] == ''){
            flash('El nombre no puede estar vacio', 'danger');
            return redirect('/EstimadorROI');
        }
        if(!isset($request['pais']) || $request['pais'] == ''){
            flash('El pais no puede estar vacio', 'danger');
            return redirect('/EstimadorROI');
        }
        
        if(!isset($request['email']) || $request['email'] == ''){
            flash('El email no puede estar vacio', 'danger');
            return redirect('/EstimadorROI');
        }elseif(!filter_var($request['email'], FILTER_VALIDATE_EMAIL)){
            flash('Formato de email incorrecto', 'danger');
            return redirect('/EstimadorROI');
        }

        $file = $this->generarDocumentos($request);

        Mail::to($request['email'],'Licensing Assurance DC Solutions')->send(new InfoEjecutivo($request['name'], $file));

        if( count(Mail::failures()) > 0 ) {
            $error = "Hubo uno o varios errores. Estos fueron: <br>";
            foreach(Mail::failures as $email_address) {
                $error += " - ".$email_address." <br>";
            }
            flash($error, 'danger');
        }else{
            flash('Se ha enviado el mensaje de manera exitosa', 'success');
        }
        //return response()->download(storage_path('documentos_generados/'.$file.'.docx'));
        return redirect('/EstimadorROI');
    }

    private function generarDocumentos(Request $request){
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/wordtemplate/LA - Portal -Business Case- ROI.docx'));

        $templateProcessor->setValue('dia', date('d').' ');
        $templateProcessor->setValue('mes', $this->getMonth(date('m')));
        $templateProcessor->setValue('ano', date('y'));

        $fab = $request['fab'];
        $array = explode(',', $fab[0]);
        $templateProcessor->setValue('CLIENTE', $request['CLIENTE']);
        $templateProcessor->setValue('Inversión_SAMaaS_DC', $request['Inversión_SAMaaS_DC']);
        $templateProcessor->setValue('Total_Valor_Agregado', $request['Total_Valor_Agregado']);
        $templateProcessor->setValue('ROI', $request['ROI']);
        $templateProcessor->setValue('mitigación_auditoria', $request['mitigación_auditoria']);
        $templateProcessor->setValue('optimización_reporte', $request['optimización_reporte']);
        $templateProcessor->setValue('facturacion', $request['facturacion']);
        $templateProcessor->setValue('administracion', $request['administracion']);
        $templateProcessor->setValue('monto_anual', $request['monto_anual']);
        $templateProcessor->setValue('costo_anual', $request['costo_anual']);
        $templateProcessor->setValue('cantidad_VM', $request['cantidad_VM']);
        $templateProcessor->setValue('fabricantes', $request['fabricantes']);



        $templateProcessor->setValue('cMicrosof', (in_array(1, $array))? '$'.$request['cobro']:'-' );
        $templateProcessor->setValue('rMicrosof', (in_array(1, $array))? '$'.$request['riesgo']:'-' );
        $templateProcessor->setValue('cVMWare', (in_array(2, $array))? '$'.$request['cobro']:'-' );
        $templateProcessor->setValue('rVMWare', (in_array(2, $array))? '$'.$request['riesgo']:'-' );
        $templateProcessor->setValue('cRedHat', (in_array(3, $array))? '$'.$request['cobro']:'-' );
        $templateProcessor->setValue('rRedHat', (in_array(3, $array))? '$'.$request['riesgo']:'-' );
        $templateProcessor->setValue('cOracle', (in_array(4, $array))? '$'.$request['cobro']:'-' );
        $templateProcessor->setValue('rOracle', (in_array(4, $array))? '$'.$request['riesgo']:'-' );
        $templateProcessor->setValue('cCitrix', (in_array(5, $array))? '$'.$request['cobro']:'-' );
        $templateProcessor->setValue('rCitrix', (in_array(5, $array))? '$'.$request['riesgo']:'-' );        
        
        $templateProcessor->setValue('cTotal', $request['cobroTotal']);
        $templateProcessor->setValue('costo_total', 10000*$request['fabricantes']);

        $templateProcessor->setValue('microsoft1', (in_array(1, $array))? '$'.$request['dolar1']:'-' );
        $templateProcessor->setValue('microsoft2', (in_array(1, $array))? '$'.$request['dolar2']:'-' );
        $templateProcessor->setValue('vmware1', (in_array(2, $array))? '$'.$request['dolar1']:'-' );
        $templateProcessor->setValue('vmware2', (in_array(2, $array))? '$'.$request['dolar2']:'-' );
        $templateProcessor->setValue('redhat1', (in_array(3, $array))? '$'.$request['dolar1']:'-' );
        $templateProcessor->setValue('redhat2', (in_array(3, $array))? '$'.$request['dolar2']:'-' );
        $templateProcessor->setValue('oracle1', (in_array(4, $array))? '$'.$request['dolar1']:'-' );
        $templateProcessor->setValue('oracle2', (in_array(4, $array))? '$'.$request['dolar2']:'-' );
        $templateProcessor->setValue('citrix1', (in_array(5, $array))? '$'.$request['dolar1']:'-' );
        $templateProcessor->setValue('citrix2', (in_array(5, $array))? '$'.$request['dolar2']:'-' );

        $templateProcessor->setValue('total_facturacion', $request['monto_anual']*1.2);
        $templateProcessor->setValue('margen_ganancia', ($request['monto_anual']*1.2)-$request['monto_anual']);
        $templateProcessor->setValue('facturación_margen', $request['facturacion']*0.05);

        $templateProcessor->setValue('SumDol', $request['SumDol']);
        $templateProcessor->setValue('OptFal', $request['OptFal']);
        
        
        
        // $templateProcessor->setValue('rTotal');


        $time = time();
        $templateProcessor->saveAs(storage_path('documentos_generados/'.$time.'.docx'));
        return $time;
    }

    private function getMonth($month){
        switch ($month) {
            case 1:
                return 'Enero';
                break;
            case 2:
                return 'Febrero';
                break;
            case 3:
                return 'Marzo';
                break;
            case 4:
                return 'Abril';
                break;
            case 5:
                return 'Mayo';
                break;
            case 6:
                return 'Junio';
                break;
            case 7:
                return 'Julio';
                break;
            case 8:
                return 'Agosto';
                break;
            case 9:
                return 'Septiembre';
                break;
            case 10:
                return 'Octubre';
                break;
            case 11:
                return 'Noviembre';
                break;
            case 12:
                return 'Diciembre';
                break;
        }
    }
}
