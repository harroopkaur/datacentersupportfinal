<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InfoEjecutivo extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $name;
    private $file;
    public function __construct($name, $file)
    {
        $this->file = $file;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        $name = $this->name;
        $email = $this->from('Info.la@licensingassurance.com')->view('mails.InfoEjecutivo')->with(['name'=>$name]);
        $email->attach(storage_path('documentos_generados/'.$this->file.'.docx'));
        return $email;
    }
}
