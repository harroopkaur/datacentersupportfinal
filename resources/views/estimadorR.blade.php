@extends('layouts.front')
@section('after_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
<style>
.active-002{color: #63c6fb !important;}
</style>
@endsection
@section('content')
<input type="hidden" name="EstimadorRr" id="EstimadorRr" value="1">
<!-- <div class="row" id="titleest">
  <div class="col-lg-12">
    <h1>
      <img src="{{asset('images/tittle2.jpg')}}" height="70" width="70"><font color="#002060">Estimador de Retorno de Inversión (ROI)</font>
    </h1>
    <div class="row">
      <h2 class="col-lg-12 text-center">
        <b><font size="6px" color="#00b0f0" face="">SAMaaS DC</font></b>
      </h2>
    </div>
  </div>
</div> -->


<div class="top-haddingsty951">
	<h4>{{ trans('index/language.estim_data0001') }}</h4>
	<hr>
	<p>{{ trans('index/language.estim_data0002') }}</p>
	<div class="clearfix"></div>
</div>


<div class="row">
  <div class="col-lg-6">
    <form method="post">
      <div class="row">
        <div class="col-md-4 parale text-left">
          <label class="paralelogramo"><b>{{ trans('index/language.estim_data0003') }}</b></label>
        </div>
      </div>





      <div class="row ptt">
        <div class="col-lg-12 lable-stydata951">
          <label>{{ trans('index/language.estim_data0004') }}</label>
        </div>
        <div class="col-lg-12">
          <input id="Vms" type="text" class="form-control fti" name="" placeholder="{{ trans('index/language.estim_data0005') }}">
        </div>
      </div>


      <div class="row ptt">
        <div class="col-lg-12 lable-stydata951">
          <label>{{ trans('index/language.estim_data0006') }}</label>
        </div>
        <div class="col-lg-12">
          <input id="AnnualReport" type="text" class="form-control fti" name="" placeholder="{{ trans('index/language.estim_data0007') }}">
        </div>
      </div>



      <div class="row ptt">
        <div class="col-lg-12 lable-stydata951">
          <label>{{ trans('index/language.estim_data0008') }}</label>
        </div>
        <div class="col-lg-12">
          <input id="CantVms" type="text" class="form-control fti" name="" placeholder="{{ trans('index/language.estim_data0009') }}">
        </div>
      </div>



      <div class="row ptt">
        <div class="col-lg-12 lable-stydata951">
          <label>{{ trans('index/language.estim_data00010') }}</label>
        </div>
        <div class="col-lg-12 frm_sty_slct">
          <select id="multiejm" class="form-control" multiple="multiple">
            <option value="1">{{ trans('index/language.estim_data00011') }}</option>
            <option value="2">{{ trans('index/language.estim_data00012') }}</option>
            <option value="3">{{ trans('index/language.estim_data00013') }}</option>
            <option value="4">{{ trans('index/language.estim_data00014') }}</option>
            <option value="5">{{ trans('index/language.estim_data00015') }}</option>
          </select>
        </div>
      </div>




      <div class="row ptt">



      	<div class="col-lg-12 calu-buttonsty951">
					<ul>
						<li><button onclick="total()" type="button" class="btn btn-primary">{{ trans('index/language.estim_data00016') }}</button></li>
						<li><button id="clear2" onclick="clearResult()" class="btn btn-primary" type="reset">{{ trans('index/language.estim_data00017') }}</button></li>
					</ul>
				</div>


      </div>



      <div class="row ptt mrg_b_15">
        <div class="col-lg-12 lable-stydata951">
          <label class="txt_cntr">{{ trans('index/language.estim_data00018') }}</label>
        </div>
        <div class="col-lg-12">
          <input id="TotalVms" disabled="disabled" type="text" class="form-control fti txt_cntr" name="" placeholder="0">
        </div>
      </div>

    </form>
  </div>





















  <div class="col-lg-6">
    <div class="row">
      <div class="col-md-4 parale text-right offset-md-8">
        <label class="paralelogramo"><b>{{ trans('index/language.estim_data00019') }}</b></label>
      </div>
    </div>
    <div class="row" style="background: #fcfcfc">
      <p class="col-lg-12" style="margin-top: 40px;">
         

          {{ trans('index/language.estim_data00032') }} 
          <b>{{ trans('index/language.estim_data00033') }}</b> 
          {{ trans('index/language.estim_data00034') }} 
          <b id="ResulRoi" class="data-boxempty"></b>, 
          {{ trans('index/language.estim_data00035') }}, 
          {{ trans('index/language.estim_data00036') }} 
          $ <b id="ResulRoi2" class="data-boxempty"></b> 
          {{ trans('index/language.estim_data00037') }}, 
          {{ trans('index/language.estim_data00038') }}  
          <b id="ResulRoi3" class="data-boxempty"></b>
      </p>

      <div class="col-lg-9 pad_0 dv0338_080619">
      	<div class="row">
		      <div class="col-lg-6 col-md-6 col-sm-6 txt-center9874">
		      	<small>{{ trans('index/language.estim_data00020') }}</small>
		      	<div class="datcenter9652"><div id="valAgu" class="valor-data695">
              <img src="{{ url('images/division.png') }}" style="position: absolute;right: -15px;bottom: 0;">
            </div></div>
		      </div>
		      <div class="col-lg-6 col-md-6 col-sm-6 txt-center9874">
		      	<small>{{ trans('index/language.estim_data00021') }}</small>
      		<div class="datcenter9652">	<div id="valInv" class="valor-data695"></div></div>
      		</div>

		      <div class="col-lg-12" style="margin-top: 30px">
			      <small>{{ trans('index/language.estim_data00022') }}</small>
			      <div class="text-center" id="valRoi"><img src="{{ url('images/equal.png') }}"><div style="text-align: center; padding-top: 30px; height: 90px; min-width: 90px; background-color: #1d337f; border-radius: 50%; border: 1px solid black; display: inline-block;"></div></div>
		      </div>


	      </div>
	    </div>

      <div class="col-lg-3 pad_0 dv1052_110619">
      	<img src="{{asset('images/roi_results_pigy.png')}}" style="width: 100%;">
      </div>






    </div>
  </div>
</div>
<br>






















<div class="row">
  <div class="col-lg-6">
    <form method="post">
      <div class="row">
        <div class="col-lg-4 parale text-left">
          <label class="paralelogramo"><b>{{ trans('index/language.estim_data00023') }}</b></label>
        </div>
      </div>








      <div class="row ptt">
        <div class="col-lg-12 lable-stydata951">
          <label>{{ trans('index/language.estim_data00024') }}</label>
        </div>
        <div class="col-lg-12">
          <input disabled="disabled" id="MitAud" type="text" class="form-control fti" name="" placeholder="0">
        </div>
      </div>


      <div class="row ptt">
        <div class="col-lg-12 lable-stydata951">
          <label>{{ trans('index/language.estim_data00025') }}</label>
        </div>
        <div class="col-lg-12">
          <input id="OptRep" disabled="disabled" type="text" class="form-control fti" name="" placeholder="0">
        </div>
      </div>


      <div class="row ptt">
        <div class="col-lg-12 lable-stydata951">
          <label>{{ trans('index/language.estim_data00026') }}</label>
        </div>
        <div class="col-lg-12">
          <input disabled="disabled" id="IncAdm" type="text" class="form-control fti" name="" placeholder="0,00 $">
      	</div>
      </div>


      <div class="row ptt">
        <div class="col-lg-12 lable-stydata951">
          <label>{{ trans('index/language.estim_data00027') }}</label>
        </div>
        <div class="col-lg-12">
					<input disabled="disabled" id="RedAdm" type="text" class="form-control fti" name="" placeholder="0">
      	</div>
      </div>


      <div class="row ptt">
        <div class="col-lg-12 lable-stydata951">
          <label class="txt_cntr">{{ trans('index/language.estim_data00028') }}</label>
        </div>
        <div class="col-lg-12">
					<input min="1" pattern="^[0-9]+" id="AgregadoROI" disabled="disabled" type="number" class="form-control fti txt_cntr" name="" placeholder="0,00 $">
      	</div>
      </div>


      <div class="row ptt">
        <div class="col-lg-12 lable-stydata951">
          <label class="txt_cntr">{{ trans('index/language.estim_data00029') }}</label>
        </div>
        <div class="col-lg-12">
					<input id="TotalRoi" disabled="disabled" type="text" class="form-control fti txt_cntr" name="" placeholder="0,00 $">
      	</div>
      </div>






    </form>
  </div>











  <div class="col-lg-6">
    <div class="row">
      <div class="col-md-4 parale text-right offset-md-8">
        <label class="paralelogramo"><b>{{ trans('index/language.estim_data00030') }}</b></label>
      </div>
    </div>
    <div class="row" style="padding-top: 40px;">
      <div id="chart_div"></div>
      <div class="col-md-12 text-center">
      	<button class="btn0458_080619" data-toggle="modal" data-target="#rcv_audit_dfnce">{{ trans('index/language.estim_data00031') }}</button>
      </div>
    </div>
  </div>
















</div>
<br><br>
@endsection
@section('after_scripts')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="{{ asset('js/total.js') }}"></script>
<script type="text/javascript">
  var TotalVmsGen;
  var TotalRoiGen;
  var enviarmail = 0;
  google.charts.load('current', {packages: ['corechart', 'bar']});
  
  function drawBasic() {
      var data = google.visualization.arrayToDataTable([
           ['Element', 'Density', { role: 'style' }],
           ['Inversion SAMaaS DC -LA', parseInt(TotalVmsGen), '#fc8800'],            // RGB value
           ['$ Valor Agregado ROI', parseInt(TotalRoiGen), '#89de00'], // CSS-style declaration
       ]);
  
      var options = {
      };
      var chart = new google.visualization.ColumnChart(
          document.getElementById('chart_div'));
  
      chart.draw(data, options);
  }
  function total(){
  	var AnnualReport = parseInt($('#AnnualReport').val());
  	var multiejm = $('#multiejm').val();
  	enviarmail = 1;
  	if(AnnualReport <= 200000){
  		if(multiejm.length == 1){
  			$('#TotalVms').val('9900');
  		}else if(multiejm.length >=2 && multiejm.length <= 5){
  			$('#TotalVms').val('14900');
  		}
  	}
  	if(AnnualReport > 200000 && AnnualReport <= 500000){
  		if(multiejm.length == 1){
  			$('#TotalVms').val('14900');
  		}else if(multiejm.length >=2 && multiejm.length <= 5){
  			$('#TotalVms').val('22900');
  		}
  	}
  	if(AnnualReport  > 500000){
  		if(multiejm.length == 1){
  			$('#TotalVms').val('19900');
  		}else if(multiejm.length >=2 && multiejm.length <= 5){
  			$('#TotalVms').val('29900');
  		}
  	}
  	$('#TotalVms').number( true, 0 );
  
  	////////////// Mitigacion Auditoria ////////////
  	var MitAud = 0;
  	for(var i=0;i<multiejm.length;i++){
  		MitAud += (0.05*AnnualReport)*0.25;
  	}
  	$('#MitAud').val(MitAud);
  	$('#MitAud').number( true, 2 );
  
  	var OptFal = 0;
  	var SumDol = 0;
  	for(var i=0;i<multiejm.length;i++){
  		SumDol += 0.05*AnnualReport;
  		OptFal += 0.03*AnnualReport;
  	}
  	$('#OptRep').val(SumDol - OptFal);
  	$('#OptRep').number( true, 2 );
  	var TotalTCO = (8000*multiejm.length) + (2000 * multiejm.length);
  	$('#RedAdm').val(TotalTCO - parseInt($('#TotalVms').val()));
  	$('#RedAdm').number( true, 2 );
  
  	$('#IncAdm').val((AnnualReport*1.2)*0.05);
  	$('#IncAdm').number( true, 2 );
  
  	$('#TotalRoi').val(parseInt($('#MitAud').val()) + parseInt($('#OptRep').val()) + parseInt($('#RedAdm').val()) + parseInt($('#IncAdm').val()));
  	$('#TotalRoi').number( true, 2 );
  
  	$('#AgregadoROI').val(Math.round(parseInt($('#TotalRoi').val())/parseInt($('#TotalVms').val())));
  	$('#AgregadoROI').number( true, 2 );
  	var TotalRoi = $('#TotalRoi').val();
  	var TotalVms = $('#TotalVms').val();
  
  
  	$('#ResulRoi').html('<font color="#212529"> <b>'+(TotalRoi/TotalVms).toFixed(1)+'X</b>.</font>');
    $('#ResulRoi2').html('<font color="#212529"><b>'+TotalVms+'</b>.</font>');
    $('#ResulRoi3').html('<font color="#212529"> <b>$ '+TotalRoi+'</b>.</font>');
  
  	$('#valAgu').html('<b><font color="#1d337f">'+((TotalRoi >= 1000)? (TotalRoi/1000).toFixed(0)+'K':TotalRoi)+'</font> <img src="{{ url('images/division.png') }}" style="position: absolute;right: -15px;bottom: 0;"></b>');
  	$('#valInv').html('<b><font color="#1d337f">'+((TotalVms >= 1000)? (TotalVms/1000).toFixed(0)+'K':TotalVms)+'</font></b>');
  	$('#valRoi').html(' <img src="{{ url('images/equal.png') }}"><div style="text-align: center; padding-top: 30px; height: 90px; min-width: 90px; background-color: #1d337f; border-radius: 50%; border: 1px solid black; display: inline-block;"><b style="color: white; font-size: large;">'+(TotalRoi/TotalVms).toFixed(1)+'X</b></div>');
  	TotalVmsGen = TotalVms;
  	TotalRoiGen = TotalRoi;
  	google.charts.setOnLoadCallback(drawBasic);
  	var cobro = 0;
  	for(var i=0;i<multiejm.length;i++){
  		cobro += 0.05*AnnualReport;
  	}
  
  	$('#mailCLIENTE').val($('#Vms').val());
  	$('#mailInversión_SAMaaS_DC').val($('#TotalVms').val());
  	$('#mailTotal_Valor_Agregado').val($('#TotalRoi').val());
  	$('#mailROI').val((TotalRoi/TotalVms).toFixed(1));
  	$('#mailmitigación_auditoria').val($('#MitAud').val());
  	$('#mailoptimización_reporte').val($('#OptRep').val());
  	$('#mailfacturacion').val($('#IncAdm').val());
  	$('#mailadministracion').val($('#RedAdm').val());
  	$('#mailmonto_anual').val($('#AnnualReport').val());
  	$('#mailcosto_anual').val(parseInt($('#AnnualReport').val())/multiejm.length);
  	$('#mailcantidad_VM').val($('#CantVms').val());
  	$('#mailfabricantes').val(multiejm.length);
  	$('#mailcobro').val(0.05*AnnualReport);
  	$('#mailriesgo').val(0.05*AnnualReport*0.25);
  	$('#mailfab').val(multiejm);
  	$('#mailcobroTotal').val(cobro);
  	$('#mailSumDol').val(SumDol);
  	$('#mailOptFal').val(OptFal);
  	
  	$('#maildolar1').val(0.05*AnnualReport);
  	$('#maildolar2').val(0.03*AnnualReport);
  	// $('#mailriesgoTotal').val($('#').val());
  	$('#send1').css('cursor', 'pointer');
  
  	$('#monto_inversion').val($("#TotalVms").val());
  	$("#total_valor_agregado").val($("#TotalRoi").val());
  	$('#representando_roi').val(Math.round(parseInt($('#TotalRoi').val())/parseInt($('#TotalVms').val())));
  
  }
  function clearResult(){
  	$('#MitAud').val("");
  	$('#OptRep').val("");
  	$('#IncAdm').val("");
  	$('#RedAdm').val("");
  	$('#AgregadoROI').val("");
  	$('#TotalRoi').val("");
  	$('#monto_inversion').val("");
  	$('#total_valor_agregado').val("");
  	$('#representando_roi').val("");
                        $('#ResulRoi').empty();
                        $('#ResulRoi2').empty();
                        $('#ResulRoi3').empty();
                        $('#valAgu').empty();
                        $('#valInv').empty();
                        $('#valRoi').empty();
                        $('#chart_div').empty();
  }
  $(document).ready(function(){
    	$('[data-toggle="tooltip"]').tooltip({trigger: 'hover'}); 
  
    	$('#send1').click(() => {
    		if(enviarmail == 1)
    			$('#formmail1').submit();
    	});
  
    	$('#multiejm').multiselect();  
  });
  
  
  $(function(){
  		// $('.format').each((index, elem) => {
  			// console.log($(elem).val());
  			$('#TotalVms').number( true, 2 );
  		$('#AnnualReport').number( true, 0 );
  		// });
  		// $('#number_container').slideDown('fast');
  		// $('#mrEnt').on('change',function(){
  		// 	console.log('Change event.');
  		// 	var val = $('#mrEnt').val();
  		// 	$('#the_number').text( val !== '' ? val : '(empty)' );
  		// });
  		// $('#mrEnt').change(function(){
  		// 	console.log('Second change event...');
  		// });
  
  		// console.log($('#mrEnt').val());
  		// $('#mrEnt').number( true, 2 );
  
  		// $('#get_number').on('click',function(){
  		// 	var val = $('#mrEnt').val();
  		// 	$('#the_number').text( val !== '' ? val : '(empty)' );
  		// });
  	});
</script>
@endsection