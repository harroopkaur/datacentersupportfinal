
<footer style="background-color: rgb(14, 94, 194);padding: 15px; width: 100%;">
	<div class="container">
	<div class="row" style="margin: 0; width: 100%">
		<div class="col-lg-3 col-md-12 dv1150_100619">
			<img src="http://datacenterla.hopto.org/images/licensing.png">
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6">
			<p class="footer_heading1">{{ trans('index/language.footer-data0001') }}</p>
			<ul class="footer_links">
				<li>
					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/SAMasaService.php">{{ trans('index/language.footer-data0002') }}</a>
				</li>
				<li>
					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/auditDefense.php">{{ trans('index/language.footer-data0003') }}</a>
				</li>
				<li>
					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/SPLA.php">{{ trans('index/language.footer-data0004') }}</a>
				</li>
				<li>
					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/deployment.php">{{ trans('index/language.footer-data0005') }}</a>
				</li>
				<li>
					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/overview.php">{{ trans('index/language.footer-data0006') }}</a>
				</li>
			</ul>
		</div>
		<div class="col-lg-3 col-md-6 col-sm-6">
			<p class="footer_heading1">{{ trans('index/language.footer-data0007') }}</p>
			<ul class="footer_links">
				<li>
					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/aboutUs.php">{{ trans('index/language.footer-data0008') }}</a>
				</li>
				<li>
					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/contact.php">{{ trans('index/language.footer-data0009') }}</a>
				</li>
				<li>
					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/articles.php">{{ trans('index/language.footer-data00010') }}</a>
				</li>
				<li>
					<a class="footer-links" target="_blank" href="#">{{ trans('index/language.footer-data00011') }}</a>
				</li>
			</ul>
		</div>
		<div class="col-lg-3 col-md-12 col-sm-12 padding-leftzero">
			<p class="footer_heading1">{{ trans('index/language.footer-data00012') }}!!</p>

			<ul class="footer_txt">
				<li>
					<span>{{ trans('index/language.footer-data00013') }}: (305)851-3545</span>
				</li>
				<li>
					<span>dcsolutions@licensingassurance.com</span>
				</li>
			</ul>

			<ul class="footer_txt">
				<li>
					<span>{{ trans('index/language.footer-data00014') }}</span>
				</li>
				<li>
					<span>16192 Coastal Highway Lewes, DE 19958</span>
				</li>
			</ul>

			<ul class="footer_txt">
				<li>
					<span>{{ trans('index/language.footer-data00015') }}</span>
				</li>
				<li class="pad_l_15">
					<span>{{ trans('index/language.footer-data00017') }} - {{ trans('index/language.footer-data00018') }}</span>
				</li>
				<li class="pad_l_15">
					<span>8:00 am - 5:00 pm</span>
				</li>
				<li class="pad_l_15">
					<span>{{ trans('index/language.footer-data00019') }}</span>
				</li>
			</ul>

		</div>
		<div class="col-lg-12 footer_copy_right">
			<p>{{ trans('index/language.footer-data00016') }}</p>
		</div>
	</div>
</div>
</footer>