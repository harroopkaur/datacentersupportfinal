<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	@php
	$navegation = 1;
	@endphp
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/x-icon" href="{{ asset('images/favicon.png') }}">
		{{-- Encrypted CSRF token for Laravel, in order for Ajax requests to work --}}
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<title>Data Center Support</title>
		@yield('before_styles')
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		 <link href="{{ asset('css') }}/bootstrap-multiselect.css" rel="stylesheet">
		 <link href="{{ asset('css') }}/timestyle.css" rel="stylesheet">
		 <link href="{{ asset('css') }}/mystyle.css" rel="stylesheet">

		 <link href="{{ asset('css') }}/owl.carousel.css" rel="stylesheet">
		 <link href="{{ asset('css') }}/owl.theme.default.css" rel="stylesheet">




		@yield('after_styles')

		
	</head>
	<body>
		<div style="height: 100%;">	  			
			@include('flash::message')
			@include('layouts.header')
  			<div>
			
				<div class="container">
					<div class="row">
						<div class="col-md-12">
					@yield('content')
				</div>

			</div>
	  			</div>
  			</div>
	
				@include('layouts.footer')
		
		</div>
		@yield('before_scripts')
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="{{ asset('js/jquery.number.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/bootstrap-multiselect.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>



		<script>
			$(document).ready(function(){
			  $('.main_banner_slider').owlCarousel({
				  items:1,
				  nav: false,
				  dots: false,
				  dotsData: true,
				  loop : true,
						autoplay:true,
						autoplayTimeout:5000,
						// autoplayHoverPause:true
				});
			});
		</script>











		<script>
			function autoType(elementClass, typingSpeed){
			  var thhis = $(elementClass);
			  // thhis.append('<div class="cursor" style="right: initial; left:0;"></div>');
			  thhis = thhis.find(".text-js");
			  var text = thhis.text().trim().split('');
			  var amntOfChars = text.length;
			  var newString = "";
			  thhis.text("");
			  setTimeout(function(){
			    thhis.css("opacity",1);
			    thhis.prev().removeAttr("style");
			    thhis.text("");
			    for(var i = 0; i < amntOfChars; i++){
			      (function(i,char){
			        setTimeout(function() {        
			          newString += char;
			          thhis.text(newString);
			        },i*typingSpeed);
			      })(i+1,text[i]);
			    }
			  },1500);
			}
			$(document).ready(function(){
			  autoType(".type-js",100);
			});
		</script>









		@yield('after_scripts')
		<footer>
		</footer>	
	</body>
</html>
