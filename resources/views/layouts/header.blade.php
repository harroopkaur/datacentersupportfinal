<div class="page-header">
<!-- 	<img src="{{ url('images/header.png') }}"> -->
	<!-- ======================== my new code =============================== -->
				<div class="main-headertop951">
					<div class="head-bordersty951 container">
						<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 top-logostymain">
							<img src="{{ url('images/logosty.png') }}">
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12 menu-topright954">
							<ul>
								<li>
									<a href="{{ url('/') }}"><img src="{{ url('images/homemenu.jpg') }}"></a>
								</li>
								<li>
									<div class="dropdown navbar-right main-buttonlanchange">
										<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"><img src="{{ url('images/language.jpg') }}">

										<span class="caret arrow-spansty"></span></button>
										<ul class="dropdown-menu lang-changesty851">
										   <li class="bandera"><a class="navbar-brand" href="lang/en"><img src="{{ url('images/usaroundflag.png') }}" class="banderaInglesa" title="English"> English</a></li>
										<li class="bandera"><a class="navbar-brand" href="lang/es"><img src="{{ url('images/spainroundflag.png') }}" class="banderaSpain" title="Spanish"> Spanish</a></li>
										</ul>
									 </div>
								</li>
							
								<li>
									<a  data-toggle="modal" data-target="#contact_us_pp" class="contact-maindatasty"><span class="cont-imgsty"><img src="{{ url('images/contactdata651.jpg') }}"></span><span class="cont-datasty">{{ trans('index/language.header_menu0001') }}</span></a>
								</li>
							</ul>
						</div>
					</div>
					</div>
			</div>

		<!-- ======================my new code close =============================== -->

		<div class="nav0408_070619" style="background: #595959;">
			<div class="container">
		<div class="row">
			

			<nav class="navbar navbar-expand-md">
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			    <span class="navbar-toggler-icon">
			    	<span></span>
			    	<span></span>
			    	<span></span>
			    </span>
			  </button>
			  <div class="collapse navbar-collapse" id="collapsibleNavbar">
			    <ul class="navbar-nav">




						<li class="nav-item">
						  <a class="nav-link active-001" href="{{ url('/') }}" id="EstimadorAu">{{ trans('index/language.header_menu0002') }}</a>
						</li>




						<li class="nav-item">
						  <a class="nav-link active-002" href="{{ url('/EstimadorROI')}}" id="EstimadorR">{{ trans('index/language.header_menu0003') }}</a>
						</li>




				    <li class="nav-item dropdown">
				      <a class="nav-link dropdown-toggle active-003" href="#" id="navbardrop" data-toggle="dropdown">
				        {{ trans('index/language.header_menu0004') }}
				      </a>
				      <div class="dropdown-menu manu-subdata954">
				        <a class="dropdown-item active-sub001" href="{{ url('/calculadoraSPLA')}}">{{ trans('index/language.header_menu0005') }}</a>
				        <a class="dropdown-item active-sub002" href="{{ url('/calculadoraSPLASQLServer')}}">{{ trans('index/language.header_menu0006') }}</a>
				      </div>
				    </li>



						<li class="nav-item">
						  <a class="nav-link" href="#" id="ConsultaLi">{{ trans('index/language.header_menu0007') }}</a>
						</li>
						<li class="nav-item">
						  <a class="nav-link" href="#" id="SmartCon">{{ trans('index/language.header_menu0008') }}</a>
						</li>
						<li class="nav-item">
						  <a class="nav-link" href="#" id="SAMaaSD">{{ trans('index/language.header_menu0009') }}</a>
						</li>
						<li class="nav-item">
						  <a class="nav-link" href="#" id="DescargaRe">{{ trans('index/language.header_menu00010') }}</a>
						</li>  
			    </ul>
			  </div>  
			</nav>


		</div>
	</div>
	</div>







<!-- <div class="tips_in_audit_wrrpr row">
	<img src="{{ url('images/imgfind.jpg') }}">
	<button data-toggle="modal" data-target="#rcv_audit_dfnce">{{ trans('index/language.header_menu00011') }}</button>
</div> -->


<div class="main_banner_slider_wrrpr">
	<div class="main_banner_slider owl-carousel owl-theme">
	  <div class="item">
	    <img src="{{ url('images/imagebanner1.jpg') }}">
	  </div>
	  <div class="item">
	    <img src="{{ url('images/imagebanner2.jpg') }}">
	  </div>
	   <div class="item">
	    <img src="{{ url('images/imagebanner3.jpg') }}">
	  </div>
	   <div class="item">
	    <img src="{{ url('images/imagebanner4.jpg') }}">
	  </div>
	</div>
	<button class="main_banner_slider_btn" data-toggle="modal" data-target="#rcv_audit_dfnce">{{ trans('index/language.header_menu00011') }}</button>
</div>
















  <!-- The Modal -->
  <div class="modal fade" id="rcv_audit_dfnce">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <!-- Modal body -->
        <div class="modal-body bdy0605_070619">
        	<button type="button" class="close dv0339_070619" data-dismiss="modal">&times;</button>
  				<form action="{{ url('/sendTIPS') }}" method="post" id="formmail">
  					{{csrf_field()}}
  					<p class="col-lg-12">
  						<font color="#002060">{{ trans('index/language.pop-data0001') }} <b>{{ trans('index/language.pop-data0002') }}</b> {{ trans('index/language.pop-data0003') }}</font>
  					</p>
						<div class="col-lg-12 dv0614_070619">
							<img src="{{asset('images/avatar.jpg')}}">
	  					<div>
  							<input type="text" class="form-control" name="name" style="width: 100%;padding: 0px" placeholder="Nombre" required>
  						</div>
						</div>
  					<div class="col-lg-12 dv0614_070619">
	  					<img src="{{asset('images/pais.jpg')}}">
	  					<div>
	  						<input type="text" class="form-control" name="pais" style="width: 100%;padding: 0px" placeholder="Pais" required>
	  					</div>	
  					</div>
  					<div class="col-lg-12 dv0614_070619">
	  					<img src="{{asset('images/email.jpg')}}">	
	  					<div>
	  						<input type="text" class="form-control" name="email" placeholder="email" required>
	  					</div>
  					</div>
  					
  						<div class="check_radio col-lg-12" style="text-align: left;padding-left: 0px;">
	  <input type="checkbox" id="chk1" value="">
	  <label for="chk1"> Send to your Inbox</label>
	</div>
	<div class="col-lg-12 pop-sendbuttonsty">
		<button id="send"> Send </button>
  				</div>
  				</form>
        </div>
      </div>
    </div>
  </div>
  
































  <!-- The Modal -->
  <div class="modal fade" id="contact_us_pp">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <!-- Modal body -->
        <div class="modal-body bdy0342_070619">
        	<button type="button" class="close dv0339_070619" data-dismiss="modal">&times;</button>
					<footer class="col-md-12" style="background-color: rgb(14, 94, 194);">
						<div class="row" style="margin: 0; width: 100%">
							<div class="col-lg-3 col-md-12 dv1150_100619">
								<img src="http://datacenterla.hopto.org/images/licensing.png">
							</div>
							<div class="col-lg-3 col-md-6 col-sm-6">
								<p class="footer_heading1">{{ trans('index/language.footer-data0001') }}</p>
								<ul class="footer_links">
									<li>
										<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/SAMasaService.php">{{ trans('index/language.footer-data0002') }}</a>
									</li>
									<li>
										<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/auditDefense.php">{{ trans('index/language.footer-data0003') }}</a>
									</li>
									<li>
										<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/SPLA.php">{{ trans('index/language.footer-data0004') }}</a>
									</li>
									<li>
										<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/deployment.php">{{ trans('index/language.footer-data0005') }}</a>
									</li>
									<li>
										<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/overview.php">{{ trans('index/language.footer-data0006') }}</a>
									</li>
								</ul>
							</div>
							<div class="col-lg-2 col-md-6 col-sm-6">
								<p class="footer_heading1">{{ trans('index/language.footer-data0007') }}</p>
								<ul class="footer_links">
									<li>
										<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/aboutUs.php">{{ trans('index/language.footer-data0008') }}</a>
									</li>
									<li>
										<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/contact.php">{{ trans('index/language.footer-data0009') }}</a>
									</li>
									<li>
										<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/articles.php">{{ trans('index/language.footer-data00010') }}</a>
									</li>
									<li>
										<a class="footer-links" target="_blank" href="#">{{ trans('index/language.footer-data00011') }}</a>
									</li>
								</ul>
							</div>
							<div class="col-lg-4 col-md-12 col-sm-12">
								<p class="footer_heading1">{{ trans('index/language.footer-data00012') }}!!</p>

								<ul class="footer_txt">
									<li>
										<span>{{ trans('index/language.footer-data00013') }}: (305)851-3545</span>
									</li>
									<li>
										<span>dcsolutions@licensingassurance.com</span>
									</li>
								</ul>

								<ul class="footer_txt">
									<li>
										<span>{{ trans('index/language.footer-data00014') }}</span>
									</li>
									<li>
										<span>16192 Coastal Highway Lewes, DE 19958</span>
									</li>
								</ul>

								<ul class="footer_txt">
									<li>
										<span>{{ trans('index/language.footer-data00015') }}</span>
									</li>
									<li class="pad_l_15">
										<span>{{ trans('index/language.footer-data00017') }} - {{ trans('index/language.footer-data00018') }}</span>
									</li>
									<li class="pad_l_15">
										<span>8:00 am - 5:00 pm</span>
									</li>
									<li class="pad_l_15">
										<span>{{ trans('index/language.footer-data00019') }}</span>
									</li>
								</ul>

							</div>
							<div class="col-lg-12 footer_copy_right">
								<p>{{ trans('index/language.footer-data00016') }}</p>
							</div>
						</div>
					</footer>
        </div>
      </div>
    </div>
  </div>
  













		<!-- ========================================================================== -->
</div>