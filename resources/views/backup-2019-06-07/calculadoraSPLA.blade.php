@extends('layouts.front')

@section('after_styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
@endsection

@section('content')
<input type="hidden" name="calcSPLA" id="calcSPLA" value="1">
	  		<div class="row" >
	  			<div class="col-lg-12">
			  		<h1 class="col-lg-712 text-center">
						<b><font color="#002060">Calculadora</font></b>
						<b><font size="34em" color="#00b0f0" face="">SPLA</font></b>
				  	</h1>
				  	<div class="row">					  	
					  	<div class="col-lg-12 text-center">
							  <font color="#7f7f7f" size="2px">Determinar las licencias requeridas de Windows Server 2016 y los costos estimados</font>
							  <img src="{{asset('images/calc.png')}}" height="70" width="70"> <font color="#7f7f7f"><b>Resultados al instante</b></font>
					  	</div>
				  	</div>
	  			</div>
				</div>
				<div class="row contitem">
					<div class="col-lg-2 mt-3">
						<select name="ediciones" id="ediciones" class="custom-select fti">
							<option value="1" selected>Data Center</option>
							<option value="2">Estandar</option>
						</select>
					</div>
					<div class="col-lg-6 text-left mt-3">
						<b><font  size="2px" color="#002060">Seleccione la Edición de Windows Server 2016</font></b>
					</div>
				</div>
	  		<div class="row contitem">
	  			<form method="post" class="col-lg-6" style="padding-top: 0px; border: 1px solid #f1f1f1; padding-bottom: 20px;padding-right: 35px;">
						<div class="row ptt">
							<div class="col-lg-4 text-left mb-4 pt-1 pb-1 alert alert-dark" role="alert" data-toggle="tooltip" data-placement="Top" title="Ingrese datos">
								<font size="2px" color="#002060">Ingrese Datos</font>
							</div>
							<div class="col-lg-8"></div>
						</div>
						
						<div class="row ptt">
							<div class="col-lg-12 text-left mb-3">
									<b><font size="2px" color="#002060">Host 1</font></b>
							</div>
							<div class="col-lg-7 text-left">
								<font size="1px" color="#002060"># de Procesadores físicos en el Host Server</font>
							</div>
							<div class="col-lg-5 pr-0">
								<input id="procesadores" type="number" step="1" min="0" class="form-control fti" name="procesadores[]" placeholder="Procesadores">
							</div>
						</div>
						<div class="row ptt">
							<div class="col-lg-7 text-left">
								<font size="1px" color="#002060"># total de Cores físicos por procesador</font>
							</div>
							<div class="col-lg-5 pr-0">
								<input id="coresFisicos" type="number" step="1" min="0" class="form-control fti" name="coresFisicos[]" placeholder="Cores físicos /procesador">
							</div>
						</div>						
						<div id="host"></div>
	  				<div class="row ptt">
	  					<div class="col-lg-2 text-left">
	  					</div>
	  					<div class="col-lg-8">
	  						<div class="row">
									<div class="col-lg-2">
											<img src="{{asset('images/calcLateral.png')}}">
									</div>
	  							<div class="col-lg-4">										
	  								<button onclick="licenciamientoGeneral()" type="button" class="btn btn-primary" style="border: 1px solid #f1f1f1;">Calcular</button>
									</div>									
	  							<div class="col-lg-4">
										<button id="clear" class="btn btn-primary" type="reset">Limpiar</button>										
									</div>
									<div class="col-lg-2">
											<img src="{{asset('images/calcDel.png')}}">
									</div>
	  						</div>
							</div>
							<div class="col-lg-2 text-left">
							</div>
	  				</div>						
	  				<div class="row ptt">
	  					<div class="col-lg-6 text-left">
	  						<b><font size="2px" color="#002060">Total Licencias Requeridas</font></b>
	  					</div>
	  					<div class="col-lg-6 pr-0">
	  						<input id="totalLicencia" type="number" min="0" name="totalLicencia" class="form-control fti format" placeholder="TOTAL Licencias Requeridas" data-toggle="tooltip" data-placement="Top" title="Cantidad de Licencias Requeridas" readonly="readonly">
	  					</div>
						</div>
						<div class="row ptt">
							<div class="col-lg-5">
								<button onclick="addHost()" class="btn btn-primary" type="button" data-toggle="tooltip" data-placement="Top" title="Agrega los Host que requieras">+ Agregar Host</button>										
							</div>
							<div class="col-lg-5">
								<button id="rHost" onclick="removeHost()" class="btn btn-warning" type="button" disabled>- Eliminar Host</button>										
							</div>
						</div>
						<div class="row ptt mt-3">
							<div style="position: relative;display: inline-block;text-align: center;">
								<img width="200" src="{{asset('images/calcBgGreen.png')}}" style="position:relative;">
								<div style="position: absolute;top: 7px;left: 10px;"><font size="2px" color="#ffffff"><b>Costo de Licenciemiento</b></font></div>
							</div>
						</div>
						<div class="row ptt">
							<div class="col-lg-12 text-left">
								<b><font size="2px" color="#002060">Licencia Base Requerida</font></b>
							</div>
							<div class="col-lg-12 text-left">
									<b><font size="2px" color="#7F7F7F">Windows Server Datacenter 2016
										</font></b>
								</div>
						</div>
						<div class="row ptt">
							<div class="col-lg-7 text-right" data-toggle="tooltip" data-placement="Top" title="Segun Datos ingresados arriba">
								<font size="2px" color="#002060">Precio Unitario/Licencia $ </font>
							</div>
							<div class="col-lg-5 pr-0">
								<input type="number" step="1" min="0" id="precioUnitario" class="form-control fti" name="precioUnitario" value="0" disabled>
							</div>
						</div>
						<div class="row ptt">
							<div class="col-lg-7 text-right" data-toggle="tooltip" data-placement="Top" title="Segun Datos ingresados arriba">
									<font size="2px" color="#002060">Costo Total Licencias Requeridas $ </font>
								</div>
								<div class="col-lg-5 pr-0">
									<input type="number" step="1" min="0" id="totalLicenciaRequerida" class="form-control fti" name="totalLicenciaRequerida" value="0" disabled>
								</div>
						</div>
						<div class="row ptt mt-3">
								<div style="position: relative;display: inline-block;text-align: center;">
									<img width="200" src="{{asset('images/calcBgGreen.png')}}" style="position:relative;">
									<div style="position: absolute;top: 7px;left: 10px;"><font size="2px" color="#ffffff"><b>Notas Adicionales</b></font></div>
								</div>
							</div>
							<div class="row ptt">
								<div class="col-lg-12 text-left">
									<b><font size="1px" color="#7F7F7F">Para un ahorro significativo en lus licencias es nesario revisar la usabilidad en el cual nuestros servicios son el fundamento, por favor contactenos para que comience a disfrutar de los beneficios de su licenciamiento al menor costo.</font></b>
								</div>
								<div class="col-lg-12 text-right">
									<a href="#" style="text-decoration:none;"><img width="50" src="{{asset('images/calcContacto.png')}}" style="position:relative;"> <b><font size="2px" color="#002060">Contactenos</font></b> </a>
								</div>
							</div>
	  			</form>
	  			<div class="col-lg-6">
					  <div style="position: relative;display: inline-block;text-align: center;">
					  <img width="400" height="260" src="{{asset('images/calcWindows.png')}}" style="position:relative;">
						  <div style="position: absolute;top: 20px;left: 10px;"> <img width="50" src="{{asset('images/calcInfo.png')}}"> <font size="3px" color="#ffffff"><b>Microsoft Windows Server 2016 DC Edition</b></font></div> 
						  <div style="position: absolute;top: 90px;left: 10px;text-align:left; width:255px;"> <font size="2px" color="#ffffff">Windows Server DC Edition requiere que todos los núcleos físicos tengan licencia para ejecutar máquinas virtuales ilimitadas (o contenedores Windows Hyper-V).</font></div> 
					  </div>
	  				<br><br>
	  				<p  class="text-left"><img src="{{asset('images/calcRequisitos.png')}}" width="30"><b><font size="3px" color="#002060"> Requisitos de Licenciamiento</font></b></p>
	  				
	  				<div>
						<p class="col-lg-12 text-left ml-5 mb-0"><img src="{{asset('images/calcList.png')}}" width="20"><font color="#002060" size="1px"> Mínimo 8 licencias básicas requeridas x procesador
							</font></p>
						<p class="col-lg-12 text-left ml-5 mb-0"><img src="{{asset('images/calcList.png')}}" width="20"><font color="#002060" size="1px">  Las licencias básicas se venden en paquetes de 2
						</font></p>
						<div class="row" style="margin-top: 8px;">
								<div class="col-lg-12">
											<img src="{{asset('images/calcClick.png')}}">
								 </div>
								<div class="col-lg-8 mb-3 mx-auto">
									<button id="bInformeResultados" class="form-control" name="bInformeResultados" style="background:#00B0F0;color:white;"> <b> Informe de Resultados</b></button>
								</div>
						</div>
		  				<form action="{{ url('/sendTIPS') }}" method="post" id="formmail" class="text-center row" style="background-color: white; border: 1px solid #f1e8e8;">
		  					{{csrf_field()}}
		  					<p class="col-lg-12"><font color="#002060">Reciba el Detalle<b>Aquí</b></font></p>
							<div class="col-lg-12 row text-center" style="padding-bottom: 10px; width: 100%;padding-right: 0px; padding-left: 103px;">

								<div class="box-header">
									<div class="input-group">
										<div class="input-group-addon">
											<img src="{{asset('images/avatar.jpg')}}" style="padding: 0px">
										</div>
										<input id="name" class="form-control" type="text" name="name" placeholder="Nombre">
									</div><!-- /.input group -->

								</div>
								<div class="box-header">
									<div class="input-group">
										<div class="input-group-addon">
											<img src="{{asset('images/pais.jpg')}}" style="padding: 0px">
										</div>
										<input id="pais" class="form-control" type="text" name="pais" placeholder="Pais">
									</div><!-- /.input group -->
								</div>
								<div class="box-header">
									<div class="input-group">
										<div class="input-group-addon">
											<img src="{{asset('images/email.jpg')}}" style="padding: 0px">
										</div>
										<input id="email" class="form-control" type="text" name="email" placeholder="Email">
									</div><!-- /.input group -->

								</div>
							</div>
		  					<p class="col-lg-12" id="send" style="cursor: pointer;">
		  						<img src="{{asset('images/send.jpg')}}">
		  						Send to your Inbox
		  						<img src="{{asset('images/inbox.jpg')}}">
		  					</p>
		  				</form>
	  				</div>
	  			</div>
	  		</div>
@endsection

@section('after_scripts')
	<script src="{{ asset('js/total.js') }}"></script>
	<script src="{{ asset('js/calculadora.js') }}"></script>
	<script>
		var count=2;
		
		var numeroMin=1;// 
		var numeroMax=19;//num = 20 host
		$(document).ready(function(){
		  	$('[data-toggle="tooltip"]').tooltip({trigger: 'hover'}); 

		  	$('#send').click(() => {
		  		$('#formmail').submit();
		  	});  
		});
		$(function(){
				$('.format').each((index, elem) => {
					$(elem).number( true, 2 );
				});
			});
	</script>
@endsection