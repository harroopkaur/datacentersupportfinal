@extends('layouts.front')

@section('after_styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
@endsection

@section('content')

 <input type="hidden" name="EstimadorRr" id="EstimadorRr" value="1">
<div class="row" id="titleest">
	  			<div class="col-lg-12">
			  		<h1>
			  			<img src="{{asset('images/tittle2.jpg')}}" height="70" width="70"><font color="#002060">Estimador de Retorno de Inversión (ROI)</font>
				  	</h1>
				  	<div class="row">
					  	<h2 class="col-lg-12 text-center">
					  		<b><font size="6px" color="#00b0f0" face="">SAMaaS DC</font></b>
					  	</h2>	
				  	</div>
	  			</div>
</div>
<div class="row">
  	<div class="col-lg-6" style="border: 1px solid #e3e2de">
  		<form method="post">
  			<div class="row">
  				<div class="col-lg-4 parale">
  				<label class="paralelogramo"><b>Ingrese Datos</b></label>
  				</div>
  			</div>
  			<div class="row" style="margin-top: 8px;" >
	  			<div class="col-lg-6 text-right" data-toggle="tooltip" data-placement="Top" title="Ingrese Nombre Proveedor de Servicios SPLA">
	  			<font size="2px" color="#002060">Nombre Proveedor de Servicios</font>
	  			 </div>
	  			<div class="col-lg-6">
	  			<input style="font-size: 10px" id="Vms" type="text" class="form-control" name="" placeholder="Ingrese el nombre del Proveedor">
	  			</div>
	  		</div>
	  		<div class="row" style="margin-top: 8px;">
	  			<div class="col-lg-6" data-toggle="tooltip" data-placement="Top" title="Ingrese Monto Anual Reportado ante Partner SPLA">
	  				<div class="row" >
	  				   <div class="col-lg-2 text-left">
	  		    			<img src="{{asset('images/form2.jpg')}}">
	  		     	   </div>
	  		     	   <div class="col-lg-10 text-right">
	  			             <font size="2px" color="#002060">Monto Anual Reportado</font>
	  				   </div>
	  			 	</div>
	  			 </div>
	  			<div class="col-lg-6">
	  			<input style="font-size: 10px" id="AnnualReport" type="text" class="form-control" name="" placeholder="Ingrese el total reportado en UDS $">
	  			</div>
	  		</div>
	  		<div class="row" style="margin-top: 8px;">
	  			<div class="col-lg-6" data-toggle="tooltip" data-placement="Top" title="Ingrese Cantidad de VMs que Administra">
	  				<div class="row">
	  					<div class="col-lg-4 text-left" >
	  		    			<img src="{{asset('images/menu3.jpg')}}">
	  		            </div>
	  		            <div class="col-lg-8 text-right">
	  			            <font size="2px" color="#002060">Cantidad de VMs</font>
	  					</div>
	  			    </div>
	  			 </div>
	  			<div class="col-lg-6">
	  			<input style="font-size: 10px" id="CantVms" type="text" class="form-control" name="" placeholder="Ingrese el total de VMs que administra">
	  			</div>
	  		</div>
	  		<div class="row" style="margin-top: 8px;">
	  			<div class="col-lg-6" data-toggle="tooltip" data-placement="Top" title="Seleccione hasta 5 Fabricantes de Software">
	  				<div class="row" >
	  				<div class="col-lg-4 text-left">
	  		    		<img src="{{asset('images/menu4.jpg')}}">
	  		    	</div>
	  		    	<div class="col-lg-8 text-right">
	  					<font id="selctfabri" style="text-align: right;" size="2px" color="#002060">Fabricantes</font>
	  				</div>
	  				</div>
	  			</div>
	  			<div class="col-lg-6">
	  				<select id="multiejm" class="form-control" multiple="multiple">
						<option value="1">Microsoft</option>
						<option value="2">VMWare</option>
						<option value="3">Red Hat</option>
						<option value="4">Oracle</option>
						<option value="5">Citrix</option>
					</select>

	  				<!-- <input style="font-size: 10px" id="Vms" type="text" class="form-control" name="" placeholder="Seleccione hasta 5 fabricantes"> -->
	  			</div>
	  		</div>
	  		<div class="row" style="margin-top: 8px;">
	  			<div class="col-lg-6" data-toggle="tooltip" data-placement="Top" title="Costo Referencial de la Solucion">
	  				<div class="row">
	  					<div class="col-lg-2 text-left" >
	  		    			<img src="{{asset('images/menu5.jpg')}}">
	  		            </div>
	  		            <div class="col-lg-10 text-right">
	  			            <b><font size="2px" color="#002060">Inversión Referencial SAMaaS DC</font></b>
	  					</div>
	  			    </div>
	  			 </div>
	  			<div class="col-lg-6">
	  			<input style="font-size: 10px" id="TotalVms" disabled="disabled" type="text" class="form-control" name="" placeholder="Ingrese el total de VMs que administra">
	  			</div>
	  		</div>

	  		<div class="row" style="margin-bottom: 15px" >
	  			<div class="col-lg-6" ></div>
					<div class="col-lg-3" data-toggle="tooltip" data-placement="Top" title="Efectuar Calculos">
						<button onclick="total()" type="button" class="btn btn-primary parale" style="border: 1px solid #f1f1f1;"><b>Calcular</b></button>
					</div>
					<div class="col-lg-3" data-toggle="tooltip" data-placement="Top" title="Limpiar Data Introducida ">
						<button id="clear2" onclick="clearResult()" style="background: white; color: #aaa4a4 " class="btn btn-primary" type="reset"><b>Limpiar</b></button>
					</div>
	  		</div>
  		</form>  	
  	</div>
  	<div class="col-lg-6" style="border: 1px solid #e3e2de">
		<div class="row">
			<div class="col-lg-4 parale">
			<label class="paralelogramo"><b>Resultados ROI</b></label>
			</div>
		</div>
		<div class="row">
			<p class="col-lg-12" style="margin-top: 40px;" id="ResulRoi">
			</p>
			<p class="col-lg-4"><small>$ Valor Agregado ROI</small></p>
			<p class="col-lg-4"><small>Inversión SAMaaS DC</small></p>
			<p class="col-lg-4"><b><font color="#002060">ROI</font></b></p>
			<div class="col-lg-4" id="valAgu" style="padding-top: 25px;">
				
			</div>
			<div class="col-lg-4" id="valInv" style="padding-top: 25px;">
				
			</div>
			<div class="col-lg-4 text-center" id="valRoi" style="padding-right: 0px;padding-left: 25px; margin-top: -25px;">
				
			</div>
		</div>  		
  	</div>

</div>
<br>

<div class="row">
  	<div class="col-lg-6" style="border: 1px solid #e3e2de">
  		<form method="post">
  			<div class="row">
  				<div class="col-lg-4 parale">
  				<label class="paralelogramo"><b>Resultados</b></label>
  				</div>
  			</div>
  			<div class="row" style="margin-top: 8px;" >
	  			<div class="col-lg-6" data-toggle="tooltip" data-placement="Top" title="Reducción de riesgos por auditorías">
	  			<div class="row" >
	  				  <div class="col-lg-2 text-left">
	  		    			<img src="{{asset('images/menu6.jpg')}}">
	  		     	   </div>
	  		           <div class="col-lg-10 text-right">
	  			            <font size="2px" color="#002060">Mitigación de Auditoría </font>
	  			       </div>
	  			 </div>
	  			 </div>

	  			<div class="col-lg-6">
	  				<div class="row" >
	  				    <div class="col-lg-11" style="padding: 0px;">
	  			<input style="font-size: 10px" disabled="disabled" id="MitAud" type="text" class="form-control" name="" placeholder="Reducción de riesgos por auditorías">
	  				</div>
	  					<div class="col-lg-1" style="padding: 0px;"></div>
	  					</div>
	  			</div>
	  		</div>
	  		<div class="row" style="margin-top: 8px;">
	  			<div class="col-lg-6" data-toggle="tooltip" data-placement="Top" title="Reducción de excedentes en reportes">
	  				<div class="row" >
	  				   <div class="col-lg-2 text-left">
	  		    			<img src="{{asset('images/menu7.jpg')}}">
	  		     	   </div>
	  		     	   <div class="col-lg-10 text-right">
	  			             <font size="2px" color="#002060">Optimización de Reporte</font>
	  				   </div>
	  			 	</div>
	  			 </div>
	  			<div class="col-lg-6">
	  				<div class="row" >
	  				    <div class="col-lg-11" style="padding: 0px;">
	  			<input style="font-size: 10px" id="OptRep" disabled="disabled" type="text" class="form-control" name="" placeholder="Reducción de excedentes en reportes">
	  				</div>
	  				<div class="col-lg-1" style="padding: 0px;"></div>
	  					</div>
	  			</div>
	  		</div>
	  		<div class="row" style="margin-top: 8px;">
	  			<div class="col-lg-6" data-toggle="tooltip" data-placement="Top" title="Control facturación usuarios finales">
	  				<div class="row">
	  					<div class="col-lg-2 text-left" >
	  		    			<img src="{{asset('images/menu8.jpg')}}">
	  		            </div>
	  		            <div class="col-lg-10 text-right">
	  			            <font size="2px" color="#002060">Incremento de Facturación</font>
	  					</div>
	  			    </div>
	  			 </div>
	  			<div class="col-lg-6">
	  				<div class="row" >
	  					<div class="col-lg-11" style="padding: 0px;">
	  			<input style="font-size: 10px" disabled="disabled" id="IncAdm" type="text" class="form-control" name="" placeholder="Control facturación usuarios finales">
	  					</div>
	  					<div class="col-lg-1" style="padding: 0px;">$</div>
					</div>
	  			</div>
	  		</div>
	  		<div class="row" style="margin-top: 8px;">
	  			<div class="col-lg-6" data-toggle="tooltip" data-placement="Top" title="Reducción del costo de administración">
	  				<div class="row" >
	  				<div class="col-lg-4 text-left">
	  		    		<img src="{{asset('images/menu9.jpg')}}">
	  		    	</div>
	  		    	<div class="col-lg-8 text-right">
	  					<font style="text-align: right;" size="2px" color="#002060">Reducción de Administración</font>
	  				</div>
	  				</div>
	  			</div>
	  			<div class="col-lg-6">
	  				<div class="row" >
	  				    <div class="col-lg-11" style="padding: 0px;">
	  				
	  				<input style="font-size: 10px" disabled="disabled" id="RedAdm" type="text" class="form-control" name="" placeholder="Reducción del costo de administración">
	  				</div>
	  				<div class="col-lg-1" style="padding: 0px;"></div>
	  					</div>
	  			</div>
	  		</div>
	  		<div class="row" style="margin-top: 8px;">
	  			<div class="col-lg-6" data-toggle="tooltip" data-placement="Top" title="Oportunidad en X cantidad de veces ROI">
	  				<div class="row">
	  					<div class="col-lg-2 text-left" >
	  		    			<img src="{{asset('images/menu5.jpg')}}">
	  		            </div>
	  		            <div class="col-lg-10 text-right">
	  			            <b><font size="2px" color="#002060">Cálculo X ROI</font></b>
	  					</div>
	  			    </div>
	  			 </div>
	  			<div class="col-lg-6">
	  				<div class="row" >
	  				    <div class="col-lg-11" style="padding: 0px;">
	  			<input style="font-size: 10px" min="1" pattern="^[0-9]+" id="AgregadoROI" disabled="disabled" type="number" class="form-control" name="" placeholder="Oportunidad en X cantidad de veces ROI">
	  					</div>
	  					<div class="col-lg-1" style="padding: 0px;">$</div>
	  			    </div>
	  			</div>
	  		</div>
	  		<div class="row" style="margin-top: 8px;">
	  			<div class="col-lg-6" data-toggle="tooltip" data-placement="Top" title="Oportunidad en USD $">
	  				<div class="row">
	  					<div class="col-lg-2 text-left" >
	  		    			<img src="{{asset('images/menu5.jpg')}}">
	  		            </div>
	  		            <div class="col-lg-10 text-right">
	  			            <b><font size="2px" color="#002060">TOTAL VALOR AGREGADO</font></b>
	  					</div>
	  			    </div>
	  			 </div>
	  			<div class="col-lg-6">
	  				<div class="row" >
	  				    <div class="col-lg-11" style="padding: 0px;">
	  			<input style="font-size: 10px" id="TotalRoi" disabled="disabled" type="text" class="form-control" name="" placeholder="Oportunidad en USD $">
	  				</div>
	  				<div class="col-lg-1" style="padding: 0px;">$</div>
	  					</div>
	  			</div>
	  		</div>
			<div class="row" style="margin-top: 8px;">
				<div class="col-lg-12 text-left">
					<b>
						<font size="2px" color="#002060">
							Con una inversión de <input class="input-border-bottom" id="monto_inversion" readonly type="text">
							se espera un ahorro <p> de USD $ <input class="input-border-bottom" id="total_valor_agregado" readonly type="text">
							representando un ROI de <input class="input-border-bottom" id="representando_roi" readonly type="text">X
						</font>
					</b>
				</div>
			</div>

	  	
  		</form>
  		
  	</div>

  	<div class="col-lg-6" style="border: 1px solid #e3e2de">
		<div class="row">
			<div class="col-lg-4 parale">
			<label class="paralelogramo"><b>Barra ROI</b></label>
			</div>
		</div>
		<div class="row">
			<div id="chart_div" style="height: 100%;"></div>
		</div>  
  	</div>

  </div>

<br><br>
@endsection

@section('after_scripts')
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script src="{{ asset('js/total.js') }}"></script>
	<script type="text/javascript">
		var TotalVmsGen;
		var TotalRoiGen;
		var enviarmail = 0;
		google.charts.load('current', {packages: ['corechart', 'bar']});

		function drawBasic() {
		    var data = google.visualization.arrayToDataTable([
		         ['Element', 'Density', { role: 'style' }],
		         ['Inversion SAMaaS DC -LA', parseInt(TotalVmsGen), '#fc8800'],            // RGB value
		         ['$ Valor Agregado ROI', parseInt(TotalRoiGen), '#89de00'], // CSS-style declaration
		     ]);

		    var options = {
		    };
		    var chart = new google.visualization.ColumnChart(
		        document.getElementById('chart_div'));

		    chart.draw(data, options);
		}
		function total(){
			var AnnualReport = parseInt($('#AnnualReport').val());
			var multiejm = $('#multiejm').val();
			enviarmail = 1;
			if(AnnualReport <= 200000){
				if(multiejm.length == 1){
					$('#TotalVms').val('9900');
				}else if(multiejm.length >=2 && multiejm.length <= 5){
					$('#TotalVms').val('14900');
				}
			}
			if(AnnualReport > 200000 && AnnualReport <= 500000){
				if(multiejm.length == 1){
					$('#TotalVms').val('14900');
				}else if(multiejm.length >=2 && multiejm.length <= 5){
					$('#TotalVms').val('22900');
				}
			}
			if(AnnualReport  > 500000){
				if(multiejm.length == 1){
					$('#TotalVms').val('19900');
				}else if(multiejm.length >=2 && multiejm.length <= 5){
					$('#TotalVms').val('29900');
				}
			}
			$('#TotalVms').number( true, 0 );

			////////////// Mitigacion Auditoria ////////////
			var MitAud = 0;
			for(var i=0;i<multiejm.length;i++){
				MitAud += (0.05*AnnualReport)*0.25;
			}
			$('#MitAud').val(MitAud);
			$('#MitAud').number( true, 2 );

			var OptFal = 0;
			var SumDol = 0;
			for(var i=0;i<multiejm.length;i++){
				SumDol += 0.05*AnnualReport;
				OptFal += 0.03*AnnualReport;
			}
			$('#OptRep').val(SumDol - OptFal);
			$('#OptRep').number( true, 2 );
			var TotalTCO = (8000*multiejm.length) + (2000 * multiejm.length);
			$('#RedAdm').val(TotalTCO - parseInt($('#TotalVms').val()));
			$('#RedAdm').number( true, 2 );

			$('#IncAdm').val((AnnualReport*1.2)*0.05);
			$('#IncAdm').number( true, 2 );

			$('#TotalRoi').val(parseInt($('#MitAud').val()) + parseInt($('#OptRep').val()) + parseInt($('#RedAdm').val()) + parseInt($('#IncAdm').val()));
			$('#TotalRoi').number( true, 2 );

			$('#AgregadoROI').val(Math.round(parseInt($('#TotalRoi').val())/parseInt($('#TotalVms').val())));
			$('#AgregadoROI').number( true, 2 );
			var TotalRoi = $('#TotalRoi').val();
			var TotalVms = $('#TotalVms').val();


			$('#ResulRoi').html('<font color="#002060">Su <b>ROI</b> será de <b>'+(TotalRoi/TotalVms).toFixed(1)+'X</b>, es decir, si invierte $ <b>'+TotalVms+'</b> en admin software, con Licensing Assurance, debe esperar un retorno de inversion de <b>$ '+TotalRoi+'</b>.</font>');

			$('#valAgu').html('<b><font color="#00b050">'+((TotalRoi >= 1000)? (TotalRoi/1000).toFixed(0)+'K':TotalRoi)+'</font> <img src="{{ url('images/division.png') }}" style="margin-left: 25px;"></b>');
			$('#valInv').html('<b><font color="#00b050">'+((TotalVms >= 1000)? (TotalVms/1000).toFixed(0)+'K':TotalVms)+'</font> <img src="{{ url('images/equal.png') }}" style="margin-left: 40px;"></b>');
			$('#valRoi').html('<div style="text-align: center; padding-top: 30px; height: 90px; width: 90px; background-color: #00b0f0; border-radius: 50%; border: 1px solid black;"><b style="color: white; font-size: large;">'+(TotalRoi/TotalVms).toFixed(1)+'X</b></div>');
			TotalVmsGen = TotalVms;
			TotalRoiGen = TotalRoi;
			google.charts.setOnLoadCallback(drawBasic);
			var cobro = 0;
			for(var i=0;i<multiejm.length;i++){
				cobro += 0.05*AnnualReport;
			}

			$('#mailCLIENTE').val($('#Vms').val());
			$('#mailInversión_SAMaaS_DC').val($('#TotalVms').val());
			$('#mailTotal_Valor_Agregado').val($('#TotalRoi').val());
			$('#mailROI').val((TotalRoi/TotalVms).toFixed(1));
			$('#mailmitigación_auditoria').val($('#MitAud').val());
			$('#mailoptimización_reporte').val($('#OptRep').val());
			$('#mailfacturacion').val($('#IncAdm').val());
			$('#mailadministracion').val($('#RedAdm').val());
			$('#mailmonto_anual').val($('#AnnualReport').val());
			$('#mailcosto_anual').val(parseInt($('#AnnualReport').val())/multiejm.length);
			$('#mailcantidad_VM').val($('#CantVms').val());
			$('#mailfabricantes').val(multiejm.length);
			$('#mailcobro').val(0.05*AnnualReport);
			$('#mailriesgo').val(0.05*AnnualReport*0.25);
			$('#mailfab').val(multiejm);
			$('#mailcobroTotal').val(cobro);
			$('#mailSumDol').val(SumDol);
			$('#mailOptFal').val(OptFal);
			
			$('#maildolar1').val(0.05*AnnualReport);
			$('#maildolar2').val(0.03*AnnualReport);
			// $('#mailriesgoTotal').val($('#').val());
			$('#send1').css('cursor', 'pointer');

			$('#monto_inversion').val($("#TotalVms").val());
			$("#total_valor_agregado").val($("#TotalRoi").val());
			$('#representando_roi').val(Math.round(parseInt($('#TotalRoi').val())/parseInt($('#TotalVms').val())));

		}
		function clearResult(){
			$('#MitAud').val("");
			$('#OptRep').val("");
			$('#IncAdm').val("");
			$('#RedAdm').val("");
			$('#AgregadoROI').val("");
			$('#TotalRoi').val("");
			$('#monto_inversion').val("");
			$('#total_valor_agregado').val("");
			$('#representando_roi').val("");
                        $('#ResulRoi').empty();
                        $('#valAgu').empty();
                        $('#valInv').empty();
                        $('#valRoi').empty();
                        $('#chart_div').empty();
		}
		$(document).ready(function(){
		  	$('[data-toggle="tooltip"]').tooltip({trigger: 'hover'}); 

		  	$('#send1').click(() => {
		  		if(enviarmail == 1)
		  			$('#formmail1').submit();
		  	});

		  	$('#multiejm').multiselect();  
		});


		$(function(){
				// $('.format').each((index, elem) => {
					// console.log($(elem).val());
					$('#TotalVms').number( true, 2 );
				$('#AnnualReport').number( true, 0 );
				// });
				// $('#number_container').slideDown('fast');
				// $('#mrEnt').on('change',function(){
				// 	console.log('Change event.');
				// 	var val = $('#mrEnt').val();
				// 	$('#the_number').text( val !== '' ? val : '(empty)' );
				// });
				// $('#mrEnt').change(function(){
				// 	console.log('Second change event...');
				// });

				// console.log($('#mrEnt').val());
				// $('#mrEnt').number( true, 2 );

				// $('#get_number').on('click',function(){
				// 	var val = $('#mrEnt').val();
				// 	$('#the_number').text( val !== '' ? val : '(empty)' );
				// });
			});
	</script>
@endsection