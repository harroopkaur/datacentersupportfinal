@extends('layouts.front')

@section('after_styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
@endsection

@section('content')

<input type="hidden" name="EstimadorAau" id="EstimadorAau" value="1">
<!-- ========================top banner start=================== -->
<div class="banner-topsty9541">
	<p>Receive our TIPS in AUDIT DEFENSE</p>
</div>
	<!-- ====================Top banner close======================== -->
	  		<div class="row" id="titleest">
	  			<div class="col-lg-12">
			  		<h1>
			  			<img src="{{asset('images/lateral01.png')}}" height="70" width="70"><font color="#002060">Estimador de Auditoria </font>
				  	</h1>
				  	<div class="row">
					  	<h1 class="col-lg-7 text-right">
					  		<b><font size="34em" color="#00b0f0" face="">SPLA</font></b>
					  	</h1>
					  	<div class="col-lg-5 text-center">
					  		<img src="{{asset('images/ensegundos.png')}}" height="50" width="50"> <font color="#7f7f7f">En segundos</font>
					  	</div>
				  	</div>
	  			</div>
	  		</div>
	  		<div class="row contitem">
	  			<form method="post" class="col-lg-6" style="padding-top: 50px; border: 1px solid #f1f1f1; padding-bottom: 20px;padding-right: 35px;">
	  				<div class="row ptt">
	  					<div class="col-lg-5 text-left" data-toggle="tooltip" data-placement="Top" title="Ingrese el total de VMs">
	  						<img src="{{asset('images/vms.jpg')}}">
	  						<b><font size="2px" color="#002060">VMs</font></b>
	  					</div>
	  					<div class="col-lg-7">
	  						<input id="Vms" type="number" step="1" min="0" class="form-control fti" name="" placeholder="Ingrese el total de VMs">
	  					</div>
	  				</div>
	  				<div class="row ptt">
	  					<div class="col-lg-5 text-left" data-toggle="tooltip" data-placement="Top" title="Ingrese el total de VMs con Windows" >
	  						<img src="{{asset('images/windows.jpg')}}">
	  						<b><font size="2px" color="#002060">VMs Windows</font></b>
	  					</div>
	  					<div class="col-lg-7">
	  						<input type="number" step="1" min="0" id="VMsWin" class="form-control fti" name="" placeholder="Ingrese el total de VMs con Windows">
	  					</div>
	  				</div>
	  				<div class="row ptt">
	  					<div class="col-lg-5 text-left" data-toggle="tooltip" data-placement="Top" title="Ingrese el total de VMs con SQL" >
	  						<img src="{{asset('images/apps.jpg')}}">
	  						<b><font size="2px" color="#002060">VMs Apps</font></b>
	  					</div>
	  					<div class="col-lg-7">
	  						<input type="number" step="1" min="0" id="VMsApp" class="form-control fti" name="" placeholder="Ingrese el total de VMs con SQL">
	  					</div>
	  				</div>
	  				<div class="row ptt">
	  					<div class="col-lg-5 text-left" data-toggle="tooltip" data-placement="Top" title="TOTAL VMs POTENCIAL">
	  						<b><font size="2px" color="#00b0f0">TOTAL VMs POTENCIAL</font></b>
	  					</div>
	  					<div class="col-lg-7">
	  						<input id="totalp" disabled="disabled" type="number" step="1" min="0" class="form-control fti bora" name="" placeholder="TOTAL VMs POTENCIAL">
	  					</div>
	  				</div>
	  				<div class="row ptt">
	  					<div class="col-lg-5 text-left" data-toggle="tooltip" data-placement="Top" title="Total VMs Escaneadas" >
	  						<img src="{{asset('images/vms_escaneadas.jpg')}}">
	  						<b><font size="1px" color="#002060">VMs Escaneadas</font></b>
	  					</div>
	  					<div class="col-lg-7">
	  						<input id="Vmsesca" type="number" step="1" min="0" class="form-control fti" name="" placeholder="Total VMs Escaneadas">
	  					</div>
	  				</div>
	  				<div class="row ptt">
	  					<div class="col-lg-5 text-left" data-toggle="tooltip" data-placement="Top" title="Total VMs No Escaneadas " >
	  						<img src="{{asset('images/vms_no_escaneadas.jpg')}}">
	  						<b><font size="1px" color="#002060">VMs No Escaneadas</font></b>
	  					</div>
	  					<div class="col-lg-7">
	  						<input id="Vmsnoes" disabled="disabled" type="number" step="1" min="0" class="form-control fti bora" name=""  placeholder="Total VMs No Escaneadas" >
	  					</div>
	  				</div>

	  				<div class="row ptt">
	  					<div class="col-lg-6 text-left">
	  					</div>
	  					<div class="col-lg-6">
	  						<div class="row">
	  							<div class="col-lg-6"><b><font size="2px" color="#002060">SQL Ent</font></b></div>
	  							<div class="col-lg-6"><b><font size="2px" color="#002060">SQL Std</font></b></div>
	  						</div>
	  					</div>
	  				</div>

	  				<div class="row ptt">
	  					<div class="col-lg-6 text-left" data-toggle="tooltip" data-placement="Top" title="Porcentaje de VMs para SQL Ent y SQL Std segun estadisticas" >
	  						<img src="{{asset('images/percent.jpg')}}">
	  						<b><font size="2px" color="#002060">Porcentaje de VMs</font></b>
	  					</div>
	  					<div class="col-lg-6">
	  						<div class="row">
	  							<div class="col-lg-5" style="padding: 0px;">
	  								<input id="pEnt" type="number" step="1" min="0" name="" class="form-control fti" value="10" max="100">
	  							</div>
	  							<div class="col-lg-1" style="padding: 0px;">%</div>
	  							<div class="col-lg-5" style="padding: 0px;">
	  								<input id="pStd" type="number" step="1" min="0" name="" class="form-control fti"  value="5" max="100">
	  							</div>
	  							<div class="col-lg-1" style="padding: 0px;">%</div>
	  						</div>
	  					</div>
	  				</div>

	  				<div class="row ptt">
	  					<div class="col-lg-6 text-left">
	  						<b><font size="2px" color="#00b0f0">TOTAL VMs</font></b>
	  					</div>
	  					<div class="col-lg-6">
	  						<div class="row">
	  							<div class="col-lg-5" style="padding: 0px;">
	  								<input id="tpEnt" type="number" step="1" min="0" name="" class="form-control fti" disabled="disabled" placeholder="SQL Ent">
	  							</div>
	  							<div class="col-lg-1" style="padding: 0px;"></div>
	  							<div class="col-lg-5" style="padding: 0px;">
	  								<input id="tpStd" type="number" step="1" min="0" name="" class="form-control fti" disabled="disabled" placeholder="SQL Std">
	  							</div>
	  							<div class="col-lg-1" style="padding: 0px;"></div>
	  						</div>
	  					</div>
	  				</div>

	  				<div class="row ptt">
	  					<div class="col-lg-6 text-left" data-toggle="tooltip" data-placement="Top" title="Normalmente entre $600 y $120" >
	  					<img src="{{asset('images/costo_mensual.jpg')}}">
	  						<b><font size="2px" color="#002060">Costo Mensual</font></b>
	  					</div>
	  					<div class="col-lg-6">
	  						<div class="row">
	  							<div class="col-lg-5" style="padding: 0px;">
	  								<input id="costoEnt" type="text" min="0" name="" class="form-control fti format" placeholder="SQL Ent" value="600">
	  							</div>
	  							<div class="col-lg-1" style="padding: 0px;">$</div>
	  							<div class="col-lg-5" style="padding: 0px;">
	  								<input id="costoStd" type="text" min="0" name="" class="form-control fti format" placeholder="SQL Std" value="120">
	  							</div>
	  							<div class="col-lg-1" style="padding: 0px;">$</div>
	  						</div>
	  					</div>
	  				</div>
	  				<div class="row ptt">
	  					<div class="col-lg-6 text-left" data-toggle="tooltip" data-placement="Top" title="Normalmente 36 meses retroactivo">
	  				<img src="{{asset('images/meses_retroactivos.jpg')}}">
	  						<b><font size="2px" color="#002060">Meses Retroactivos</font></b>
	  					</div>
	  					<div class="col-lg-6">
	  						<div class="row">
	  							<div class="col-lg-5" style="padding: 0px;">
	  								<input id="mrEnt" type="number" min="0" name="" class="form-control fti" placeholder="SQL Ent" value="36">
	  							</div>
	  							<div class="col-lg-1" style="padding: 0px;"></div>
	  							<div class="col-lg-5" style="padding: 0px;">
	  								<input id="mrStd" type="number" min="0" name="" class="form-control fti" placeholder="SQL Std" value="36">
	  							</div>
	  							<div class="col-lg-1" style="padding: 0px;"></div>
	  						</div>
	  					</div>
	  				</div>
	  				<div class="row ptt">
	  					<div class="col-lg-6 text-left" data-toggle="tooltip" data-placement="Top" title="Monto para SQL Ent y SQL Std" >
	  				<img src="{{asset('images/monto.jpg')}}">
	  						<b><font size="2px" color="#002060">Monto</font></b>
	  					</div>
	  					<div class="col-lg-6">
	  						<div class="row">
	  							<div class="col-lg-5" style="padding: 0px;">
	  								<input id="monEnd" type="text" min="0" name="" class="form-control bora fti format" readonly="readonly">
	  							</div>
	  							<div class="col-lg-1" style="padding: 0px;">$</div>
	  							<div class="col-lg-5" style="padding: 0px;">
	  								<input id="monStd" type="text" min="0" name="" class="form-control bora fti format" data-toggle="tooltip" data-placement="Top" title="Monto para SQL Std" readonly="readonly">
	  							</div>
	  							<div class="col-lg-1" style="padding: 0px;">$</div>
	  						</div>
	  					</div>
	  				</div>

	  				<div class="row ptt">
	  					<div class="col-lg-6 text-left">
	  					</div>
	  					<div class="col-lg-6">
	  						<div class="row">
	  							<div class="col-lg-6">
	  								<button onclick="total()" type="button" class="btn btn-primary" style="border: 1px solid #f1f1f1;">Calcular</button>
	  							</div>
	  							<div class="col-lg-6">
	  								<button id="clear" class="btn btn-primary" type="reset">Limpiar</button>
	  							</div>
	  						</div>
	  					</div>
	  				</div>

	  				<div class="row ptt">
	  					<div class="col-lg-6 text-left">
	  						<b><font size="3px" color="#00b0f0">Cobro Estimado de Auditoría</font></b>
	  					</div>
	  					<div class="col-lg-6">
	  						<input id="cstp" type="text" min="0" name="" class="form-control fti format" placeholder="TOTAL VMs POTENCIAL" disabled="disabled">
	  					</div>
	  				</div>
	  			</form>
	  			<div class="col-lg-6" style="background-color: #f1f1f1">
	  				<iframe width="400" height="260"
						src="https://www.youtube.com/embed/OXZuqujL3nY">
					</iframe> 
	  				<br><br>
	  				<br>
	  				<font>En la presente Auditoría Ud. corre el riesgo de pagar aproximadamente USD $ <span id="montss">_____</span>
                    Podemos ayudarle a reducir este monto. Póngase en contacto con nuestros especialistas.
                    </font>
	  				
	  				
	  				<br><br>
	  				<div style="padding-left: 30px; padding-right: 30px;">
		  				<h6 style="background-color: rgb(14, 94, 194); border-radius: 27px;">
		  					<img src="{{asset('images/top10.png')}}" style="border-radius: 100%;" height="50">
		  					<b style="color: white;">Principales pasos en una Auditoria</b>
		  				</h6>
		  				<form action="{{ url('/sendTIPS') }}" method="post" id="formmail" class="text-center row" style="background-color: white; border: 1px solid #f1e8e8;">
		  					{{csrf_field()}}
		  					<p class="col-lg-12"><font color="#002060">Completa los datos para recibir <b>TIPS</b> de defensa en las <b>auditorías</b></font></p>
		  					<div class="col-lg-12 row text-center" style="padding-bottom: 10px; width: 100%;    padding-right: 0px;">
		  						<div class="col-lg-6 row">
		  							<div class="col-lg-4" style="padding: 0px;">
		  								<img src="{{asset('images/avatar.jpg')}}" style="padding: 0px">
				  					</div>
				  					<div class="col-lg-8" style="padding: 0px;">
			  							<input type="text" class="form-control" name="name" style="width: 100%;padding: 0px" placeholder="Nombre" required>
			  						</div>
		  						</div>
			  					<div class="col-lg-6 row" style="padding-right: 0px;">
				  					<div class="col-lg-4" style="padding: 0px;">
				  						<img src="{{asset('images/pais.jpg')}}" style="padding: 0px">
				  					</div>
				  					<div class="col-lg-8" style="padding: 0px;">
				  						<input type="text" class="form-control" name="pais" style="width: 100%;padding: 0px" placeholder="Pais" required>					  						
				  					</div>	
			  					</div>
		  					</div>
		  					<div class="col-lg-12 row" style="padding-right: 0px;">
			  					<div style="margin-left: 4%">
			  						<img src="{{asset('images/email.jpg')}}">	
			  					</div>
			  					<div style="width: 74%;">
			  						<input type="text" class="form-control" name="email" placeholder="email" required>
			  					</div>
		  					</div>
		  					<p class="col-lg-12" id="send" style="cursor: pointer;">
		  						<img src="{{asset('images/send.jpg')}}">
		  						Send to your Inbox
		  						<img src="{{asset('images/inbox.jpg')}}">
		  					</p>
		  				</form>
	  				</div>
	  			</div>
	  		</div>
@endsection

@section('after_scripts')
	<script src="{{ asset('js/total.js') }}"></script>
	<script>
		$(document).ready(function(){
		  	$('[data-toggle="tooltip"]').tooltip({trigger: 'hover'}); 

		  	$('#send').click(() => {
		  		$('#formmail').submit();
		  	});  
		});
		$(function(){
				$('.format').each((index, elem) => {
					$(elem).number( true, 2 );
				});
			});
	</script>
@endsection