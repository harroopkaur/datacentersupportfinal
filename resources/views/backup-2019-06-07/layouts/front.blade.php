<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/x-icon" href="{{ asset('images/favicon.png') }}">
		{{-- Encrypted CSRF token for Laravel, in order for Ajax requests to work --}}
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<title>Data Center Support</title>
		@yield('before_styles')
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		 <link href="{{ asset('css') }}/bootstrap-multiselect.css" rel="stylesheet">
		 <link href="{{ asset('css') }}/timestyle.css" rel="stylesheet">
		 <link href="{{ asset('css') }}/mystyle.css" rel="stylesheet">
		@yield('after_styles')

		
	</head>
	<body>
		<div class="container" style="height: 100%; border: 1px solid #f1f1f1;">	  			
			@include('flash::message')
			@include('layouts.header')
  			<div class="row">
			
				<div class="col-lg-12 text-center">
					@yield('content')
	  			</div>
  			</div>
			<div class="row" style="margin-left: 0px !important;margin-right: 0px !important;">
				@include('layouts.footer')
			</div>
		</div>
		@yield('before_scripts')
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="{{ asset('js/jquery.number.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/bootstrap-multiselect.js') }}"></script>
		@yield('after_scripts')
		<footer>
		</footer>	
	</body>
</html>
