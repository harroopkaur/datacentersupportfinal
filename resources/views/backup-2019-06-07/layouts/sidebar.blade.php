<style>
    .dropdown:hover > .dropdown-menu {
        display: block;
    }
    .dropdown > .dropdown-toggle:active {
        /*Without this, clicking will make it sticky*/
        pointer-events: none;
    }
</style>
<div class="col-lg-12" style="border-style: solid; border-width: 0px 1px 1px 1px; border-color: #f1f1f1;">
  	<div class="row" style="height: 80px;"></div>












    
	<div class="row pt">
  		<div class="col-lg-3" style="padding-right: 0px;">
  			<img src="{{asset('images/lateral01.png')}}" height="40" width="40">
  		</div>
  		<div class="col-lg-9" style="padding-left: 0px;padding-top: 5px;">
  			<a href="{{ url('/') }}" id="EstimadorAu"><font size="2px">Estimador Auditoria</font></a>
  		</div>
  	</div>
	<div class="row pt">
  		<div class="col-lg-3" style="padding-right: 0px;">
  			<img src="{{asset('images/calcLateral.png')}}" height="40" width="40">
  		</div>
  		<div class="col-lg-9" style="padding-left: 0px;padding-top: 5px;">
            <div class="dropdown">
                <a href="#" id="CalculadoraS" data-toggle="dropdown"><font size="2em">Calculadora SPLA</font></a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ url('/calculadoraSPLA')}}">Windows Server</a>
                    <a class="dropdown-item" href="{{ url('/calculadoraSPLASQLServer')}}">SQL Server</a>
                </div>
            </div>
  		</div>
  	</div>
  <div class="row pt">
      <div class="col-lg-3" style="padding-right: 0px;">
        <img src="{{asset('images/ROI.jpg')}}" height="40" width="40">
      </div>
      <div class="col-lg-9 " style="padding-left: 0px;padding-top: 5px;">
        <a href="/EstimadorROI" id="EstimadorR"><font size="2em">Estimador ROI</font></a>
      </div>
    </div>
	<div class="row pt">
  		<div class="col-lg-3" style="padding-right: 0px;">
  			<img src="{{asset('images/lateral03.png')}}" height="40" width="40">
  		</div>
  		<div class="col-lg-9" style="padding-left: 0px;padding-top: 5px;">
  			<a href="#" id="ConsultaLi"><font size="2em">Consulta Licenciamiento</font></a>
  		</div>
  	</div>
	<div class="row pt">
  		<div class="col-lg-3" style="padding-right: 0px;">
  			<img src="{{asset('images/lateral05.png')}}" height="40" width="40">
  		</div>
  		<div class="col-lg-9" style="padding-left: 0px;padding-top: 5px;">
  			<a href="#" id="SmartCon"><font size="2em">Smart Control DC</font></a>
  		</div>
  	</div>
	<div class="row pt">
  		<div class="col-lg-3" style="padding-right: 0px;">
  			<img src="{{asset('images/lateral04.png')}}" height="40" width="40">
  		</div>
  		<div class="col-lg-9" style="padding-left: 0px;padding-top: 5px;">
  			<a href="#" id="DescargaRe" ><font size="2em">Descarga de Recursos</font></a>
  		</div>
  	</div>
    <div class="row pt">
      <div class="col-lg-3" style="padding-right: 0px;">
        <img src="{{asset('images/SAMaa.jpg')}}" height="40" width="40">
      </div>
      <div class="col-lg-9" style="padding-left: 0px;padding-top: 5px;">
        <a href="#" id="SAMaaSD" ><font size="2em">SAMaaS DC</font></a>
      </div>
    </div>
    <div id="EstimadorRoiMenu">
      <div class="col-lg-12 text-center">
          <a href="#formmail"><img src="{{asset('images/menu10.jpg')}}"></a>
      </div>

      <br><br>
      <div class="col-lg-12 text-center">

        <label class="parale rounded" id="infejec"> Informe Ejecutivo</label>
        
      </div>

      <div style="padding-left: 30px; padding-right: 30px;">
              
              <form action="{{ url('/sendInfoEjecutivo') }}" method="post" id="formmail1" class="text-center row" style="background-color: white; border: 1px solid #f1e8e8;">
                {{csrf_field()}}
                <p class="col-lg-12"><font color="#002060">Recibe el detalle <b>Aqui</b></font></p>
                  <div class="col-lg-12 row text-center" style=" width: 100%;padding-right: 0px; padding-left: 0px;">

                      <div class="box-header">
                          <div class="input-group">
                              <div class="input-group-addon">
                                  <img src="{{asset('images/avatar.jpg')}}" style="padding: 0px">
                              </div>
                              <input id="name" class="form-control" type="text" name="name" placeholder="Nombre">
                          </div><!-- /.input group -->

                      </div>
                      <div class="box-header">
                          <div class="input-group">
                              <div class="input-group-addon">
                                  <img src="{{asset('images/pais.jpg')}}" style="padding: 0px">
                              </div>
                              <input id="pais" class="form-control" type="text" name="pais" placeholder="Pais">
                          </div><!-- /.input group -->
                      </div>
                      <div class="box-header">
                          <div class="input-group">
                              <div class="input-group-addon">
                                  <img src="{{asset('images/email.jpg')}}" style="padding: 0px">
                              </div>
                              <input id="email" class="form-control" type="text" name="email" placeholder="Email">
                          </div><!-- /.input group -->

                      </div>
                  </div>
                <p class="col-lg-12"  style="cursor: pointer;">
                <div style="padding-bottom: 10px; margin-left: 2%;" class="row" id="send1">
                  <img src="{{asset('images/send.jpg')}}">
                  Send to your Inbox
                  <img src="{{asset('images/inbox.jpg')}}">
                </div>
                </p>
                <input type="hidden" name="CLIENTE" id="mailCLIENTE">
                <input type="hidden" name="Inversión_SAMaaS_DC" id="mailInversión_SAMaaS_DC">
                <input type="hidden" name="Total_Valor_Agregado" id="mailTotal_Valor_Agregado">
                <input type="hidden" name="ROI" id="mailROI">
                <input type="hidden" name="mitigación_auditoria" id="mailmitigación_auditoria">
                <input type="hidden" name="optimización_reporte" id="mailoptimización_reporte">
                <input type="hidden" name="facturacion" id="mailfacturacion">
                <input type="hidden" name="administracion" id="mailadministracion">
                <input type="hidden" name="monto_anual" id="mailmonto_anual">
                <input type="hidden" name="costo_anual" id="mailcosto_anual">
                <input type="hidden" name="cantidad_VM" id="mailcantidad_VM">
                <input type="hidden" name="fabricantes" id="mailfabricantes">
                <input type="hidden" name="cobro" id="mailcobro">
                <input type="hidden" name="riesgo" id="mailriesgo">
                <input type="hidden" name="cobroTotal" id="mailcobroTotal">
                <input type="hidden" name="SumDol" id="mailSumDol">
                <input type="hidden" name="OptFal" id="mailOptFal">
                <input type="hidden" name="dolar1" id="maildolar1">
                <input type="hidden" name="dolar2" id="maildolar2">
                {{-- <input type="hidden" name="riesgoTotal" id="mailriesgoTotal"> --}}
                <input type="hidden" name="fab[]" id="mailfab">
              </form>
            </div>

    </div>
</div> 

