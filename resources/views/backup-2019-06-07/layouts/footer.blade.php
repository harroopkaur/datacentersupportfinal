<footer class="row" style="background-color: rgb(14, 94, 194);">
	  			<div class="col-lg-3">
	  				<img src="{{asset('images/licensing.png')}}">
	  			</div>
	  			<div class="col-lg-3">
	  				<p style="color: white;"><font size="3em">Our Products</font></p>
	  				<p style="line-height: 1; color: white;">
	  					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/SAMasaService.php">SAM as a Service</a><br>
	  					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/auditDefense.php">Audit Defense</a><br>
	  					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/SPLA.php">SAM as a Service for SPLA</a> <br>
	  					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/deployment.php">Deployment Diagnostic</a> <br>
	  					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/overview.php">Overview</a>
	  				</p>
	  			</div>
	  			<div class="col-lg-2">
	  				<p style="color: white;"><font size="3em">Our Team</font></p>
	  				<p style="line-height: 1; color: white;">
	  					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/aboutUs.php">About Us</a><br>
	  					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/contact.php">Contact Us</a><br>
	  					<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/articles.php">Article</a><br>
	  					<a class="footer-links" target="_blank" href="#">Videos</a>
	  				</p>
	  			</div>
	  			<div class="col-lg-1">
	  				<img src="{{asset('images/contact.png')}}" height="90" width="80" style="padding-top: 10px;">
	  			</div>
	  			<div class="col-lg-3">
	  				<p style="color: white;"><font size="3em">CONTACT US!!</font></p>
	  				<p style="line-height: 1; color: white;">
	  					<font size="2em">Phone: (305)851-3545</font><br>
	  					<font size="2em">dcsolutions@licensingassurance.com</font>
	  				</p>
	  				<p style="line-height: 1; color: white;">
	  					<font size="2em">Licensing Assurance LLC</font> <br>
	  					<font size="2em">16192 Coastal Highway</font> <br>
	  					<font size="2em">Lewes, DE 19958</font>
	  				</p>
	  				<p style="line-height: 1; color: white;">
	  					<font size="2em">We are available</font><br>
	  					<font size="2em">Monday - Friday</font><br>
	  					<font size="2em">8:00 am - 5:00 pm</font><br>
	  					<font size="2em">East Time</font>
	  				</p>
	  				<p style="color: white;">
	  					<font size="2em">Follow us and Share</font>
	  				</p>
	  			</div>
	  			<div class="col-lg-12" style="color: white; margin-top: -5%">
	  				<font size="2em">Copyright Licensing Assurance LLC. All Rights Reserved</font>
	  			</div>
	  		</footer>