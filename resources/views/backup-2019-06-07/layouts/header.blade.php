<div class="page-header">
<!-- 	<img src="{{ url('images/header.png') }}"> -->
	<!-- ======================== my new code =============================== -->
				<div class="main-headertop951">
					<div class="head-bordersty951">
						<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12 top-logostymain">
							<img src="{{ url('images/logosty.jpg') }}">
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12 menu-topright954">
							<ul>
								<li>
									<a href=""><img src="{{ url('images/homemenu.jpg') }}"></a>
								</li>
								<li>
									<div class="dropdown navbar-right main-buttonlanchange">
										<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"><img src="{{ url('images/language.jpg') }}">

										<span class="caret arrow-spansty"></span></button>
										<ul class="dropdown-menu lang-changesty851">
										   <li class="bandera"><a class="navbar-brand" href="#"><img src="{{ url('images/usaroundflag.png') }}" class="banderaInglesa" title="English"> English</a></li>
										<li class="bandera"><a class="navbar-brand" href="#"><img src="{{ url('images/spainroundflag.png') }}" class="banderaSpain" title="Spanish"> Spanish</a></li>
										</ul>
									 </div>
								</li>
								<li>
									<a  data-toggle="modal" data-target="#contact_us_pp" class="contact-maindatasty"><span class="cont-imgsty"><img src="{{ url('images/contactdata651.jpg') }}"></span><span class="cont-datasty">Contact us</span></a>
								</li>
							</ul>
						</div>
					</div>
					</div>
			</div>

		<!-- ======================my new code close =============================== -->

		<div class="nav0408_070619">
		<div class="row">
			

			<nav class="navbar navbar-expand-md">
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			    <span class="navbar-toggler-icon"></span>
			  </button>
			  <div class="collapse navbar-collapse" id="collapsibleNavbar">
			    <ul class="navbar-nav">




						<li class="nav-item">
						  <a class="nav-link" href="{{ url('/') }}" id="EstimadorAu">Audit Estimator</a>
						</li>




						<li class="nav-item">
						  <a class="nav-link" href="/EstimadorROI" id="EstimadorR">ROI estimator</a>
						</li>




				    <li class="nav-item dropdown">
				      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
				        SPLA Calculator
				      </a>
				      <div class="dropdown-menu manu-subdata954">
				        <a class="dropdown-item" href="{{ url('/calculadoraSPLA')}}">Windows Server</a>
				        <a class="dropdown-item" href="{{ url('/calculadoraSPLASQLServer')}}">SQL Server</a>
				      </div>
				    </li>



						<li class="nav-item">
						  <a class="nav-link" href="#" id="ConsultaLi">Licensing consultation</a>
						</li>
						<li class="nav-item">
						  <a class="nav-link" href="#" id="SmartCon">Smart Control DC</a>
						</li>
						<li class="nav-item">
						  <a class="nav-link" href="#" id="SAMaaSD">SAMaaS DC</a>
						</li>
						<li class="nav-item">
						  <a class="nav-link" href="#" id="DescargaRe">Resources Download</a>
						</li>  
			    </ul>
			  </div>  
			</nav>


		</div>
	</div>



















































  <!-- The Modal -->
  <div class="modal fade" id="contact_us_pp">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <!-- Modal body -->
        <div class="modal-body bdy0342_070619">
        	<button type="button" class="close dv0339_070619" data-dismiss="modal">&times;</button>
					<footer class="col-md-12" style="background-color: rgb(14, 94, 194);">
						<div class="row">
							<div class="col-lg-3">
								<img src="http://datacenterla.hopto.org/images/licensing.png">
							</div>
							<div class="col-lg-3">
								<p style="color: white;"><font size="3em">Our Products</font></p>
								<p style="line-height: 1; color: white;">
									<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/SAMasaService.php">SAM as a Service</a><br>
									<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/auditDefense.php">Audit Defense</a><br>
									<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/SPLA.php">SAM as a Service for SPLA</a> <br>
									<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/deployment.php">Deployment Diagnostic</a> <br>
									<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/overview.php">Overview</a>
								</p>
							</div>
							<div class="col-lg-2">
								<p style="color: white;"><font size="3em">Our Team</font></p>
								<p style="line-height: 1; color: white;">
									<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/aboutUs.php">About Us</a><br>
									<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/contact.php">Contact Us</a><br>
									<a class="footer-links" target="_blank" href="https://www.licensingassurance.com/articles.php">Article</a><br>
									<a class="footer-links" target="_blank" href="#">Videos</a>
								</p>
							</div>
							<div class="col-lg-1">
								<img src="http://datacenterla.hopto.org/images/contact.png" height="90" width="80" style="padding-top: 10px;">
							</div>
							<div class="col-lg-3">
								<p style="color: white;"><font size="3em">CONTACT US!!</font></p>
								<p style="line-height: 1; color: white;">
									<font size="2em">Phone: (305)851-3545</font><br>
									<font size="2em">dcsolutions@licensingassurance.com</font>
								</p>
								<p style="line-height: 1; color: white;">
									<font size="2em">Licensing Assurance LLC</font> <br>
									<font size="2em">16192 Coastal Highway</font> <br>
									<font size="2em">Lewes, DE 19958</font>
								</p>
								<p style="line-height: 1; color: white;">
									<font size="2em">We are available</font><br>
									<font size="2em">Monday - Friday</font><br>
									<font size="2em">8:00 am - 5:00 pm</font><br>
									<font size="2em">East Time</font>
								</p>
								<p style="color: white;">
									<font size="2em">Follow us and Share</font>
								</p>
							</div>
							<div class="col-lg-12" style="color: white; margin-top: -5%">
								<font size="2em">Copyright Licensing Assurance LLC. All Rights Reserved</font>
							</div>
						</div>
					</footer>
        </div>
      </div>
    </div>
  </div>
  

		<!-- ========================================================================== -->
</div>