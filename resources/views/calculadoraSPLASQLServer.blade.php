@extends('layouts.front')
@section('after_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
<style>
.active-003{color: #63c6fb !important;}
.active-sub002{color: #63c6fb !important;}
</style>

@endsection
@section('content')
<input type="hidden" name="calcSPLA" id="calcSPLA" value="1">
<div class="top-haddingsty951">
  <h4>{{ trans('index/language.splasql-data0001') }}</h4>
  <hr>
  <div class="clearfix"></div>
  <p>{{ trans('index/language.splasql-data0002') }}</p>
</div>
<div class="row contitem">
  <form method="post" class="col-lg-6" style=" padding-bottom: 20px;">
    <div class="row contitem ptt">
      <div class="col-lg-12 lable-stydata951">
        <label>{{ trans('index/language.splasql-data0004') }}</label>
      </div>
      <div class="col-lg-12">
        <select class="form-control fti" name="ediciones" id="ediciones" onchange="onchangeEdiciones(this.value)" class="custom-select fti">
          <option value="1" selected>{{ trans('index/language.splasql-data0005') }}</option>
          <option value="2">{{ trans('index/language.splasql-data0006') }}</option>
          <option value="3">{{ trans('index/language.splasql-data0007') }}</option>
        </select>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 parale">
        <label class="paralelogramo"><b>{{ trans('index/language.splasql-data0008') }}</b></label>
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label># {{ trans('index/language.splasql-data0009') }}</label>
      </div>
      <div class="col-lg-12">
        <input id="cantidadHost" type="number" step="1" min="0" class="form-control fti" name="cantidadhost[]" placeholder="{{ trans('index/language.splasql-data00010') }}">
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label># {{ trans('index/language.splasql-data00011') }}</label>
      </div>
      <div class="col-lg-12">
        <input id="cpuPorHost" type="number" step="1" min="0" class="form-control fti" name="coreshost[]" placeholder="{{ trans('index/language.splasql-data00012') }}">
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label># {{ trans('index/language.splasql-data00013') }}</label>
      </div>
      <div class="col-lg-12">
        <input id="coresPorCPU" type="number" step="1" min="0" class="form-control fti" name="corescpu[]" placeholder="{{ trans('index/language.splasql-data00014') }}">
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label>{{ trans('index/language.splasql-data00015') }}</label>
      </div>
      <div class="col-lg-12">
        <input id="vmsql" type="number" value="" class="form-control fti" name="vmsql[]" placeholder="{{ trans('index/language.splasql-data00016') }}">
      </div>
    </div>
    <div id="vmsqldata"></div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label>{{ trans('index/language.splasql-data00017') }} <span id="edicion-selected">{{ trans('index/language.splasql-data00018') }}</span> {{ trans('index/language.splasql-data00019') }}  $</label>
      </div>
      <div class="col-lg-12">
        <input type="text" step="1" min="0" id="costolicencia" class="form-control fti" name="costolicencia" readonly value="600">
      </div>
    </div>
    <div id="host"></div>
    <div class="row ptt">
      <div class="col-lg-12 calu-buttonsty951">
        <ul>
          <li><button onclick="calcularSpla()" type="button" class="btn btn-primary" style="border: 1px solid #f1f1f1;">{{ trans('index/language.splasql-data00020') }}</button></li>
          <li><button onclick="limpiarSpla()" id="clear" class="btn btn-primary" type="reset">{{ trans('index/language.splasql-data00021') }}</button></li>
        </ul>
      </div>
    </div>
    <div class="row" style="margin-top: 20px">
      <div class="col-lg-4 parale">
        <label class="paralelogramo"><b>{{ trans('index/language.splasql-data00022') }}</b></label>
      </div>
    </div>





    <div class="row ptt">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="row">
          <div class="col-lg-12 lable-stydata951">
            <label>{{ trans('index/language.splasql-data00023') }}</label>
          </div>
        </div>
        <div class="row ptt">
          <div class="col-lg-12 lable-stydata951">
            <label># {{ trans('index/language.splasql-data00024') }}</label>
          </div>
          <div class="col-lg-12">
            <input id="coresovcores1" type="text" step="1" min="0" name="" class="form-control fti" placeholder="{{ trans('index/language.splasql-data00025') }}" readonly value="" max="100">
          </div>
        </div>
        <div class="row ptt">
          <div class="col-lg-12 lable-stydata951">
            <label># {{ trans('index/language.splasql-data00026') }}</label>
          </div>
          <div class="col-lg-12">
            <input id="vms1" type="text" step="1" min="0" name="" class="form-control fti" placeholder="{{ trans('index/language.splasql-data00027') }}" readonly value="" max="100">
          </div>
        </div>
        <div class="row ptt">
          <div class="col-lg-12 lable-stydata951">
            <label># {{ trans('index/language.splasql-data00028') }}</label>
          </div>
          <div class="col-lg-12">
            <input id="2coresrequeridas1" type="text" step="1" min="0" name="" class="form-control fti" placeholder="{{ trans('index/language.splasql-data00029') }}" readonly value="" max="100">
          </div>
        </div>
        <div class="row ptt">
          <div class="col-lg-12 lable-stydata951">
            <label>{{ trans('index/language.splasql-data00030') }}</label>
          </div>
          <div class="col-lg-12">
            <input id="costototal1" type="text" readonly step="1" min="0" name="" class="form-control fti" value="" max="100">
          </div>
        </div>
      </div>




      
      <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="row">
          <div class="col-lg-12 lable-stydata951">
            <label>{{ trans('index/language.splasql-data00031') }}</label>
          </div>
        </div>
        <div class="row ptt">
          <div class="col-lg-12 lable-stydata951">
            <label># {{ trans('index/language.splasql-data00032') }}</label>
          </div>
          <div class="col-lg-12">
            <input id="coresovcores2" type="text" step="1" min="0" name="" class="form-control fti" placeholder="{{ trans('index/language.splasql-data00033') }}" readonly value="" max="100">
          </div>
        </div>
        <div class="row ptt">
          <div class="col-lg-12 lable-stydata951">
            <label># {{ trans('index/language.splasql-data00034') }}</label>
          </div>
          <div class="col-lg-12">
            <input id="vms2" type="text" step="1" min="0" name="" class="form-control fti" placeholder="{{ trans('index/language.splasql-data00035') }}" readonly value="" max="100">
          </div>
        </div>
        <div class="row ptt">
          <div class="col-lg-12 lable-stydata951">
            <label># {{ trans('index/language.splasql-data00036') }}</label>
          </div>
          <div class="col-lg-12">
            <input id="2coresrequeridas2" type="text" step="1" min="0" name="" class="form-control fti" placeholder="{{ trans('index/language.splasql-data00037') }}" readonly value="" max="100">
          </div>
        </div>
        <div class="row ptt">
          <div class="col-lg-12 lable-stydata951">
            <label>{{ trans('index/language.splasql-data00038') }}</label>
          </div>
          <div class="col-lg-12">
            <input id="costototal2" type="text" readonly step="1" min="0" name="" class="form-control fti" value="" max="100">
          </div>
        </div>
      </div>
    </div>





    <div class="row" style="padding-top: 40px;">
      <div id="chart_div"></div>
      <div class="col-md-12 text-center">
        <a class="btn0458_080619" data-toggle="modal" data-target="#rcv_audit_dfnce">{{ trans('index/language.splasql-data00039') }}</a>
      </div>
    </div>
  </form>
  <div class="col-lg-6">
    <div class="cal-databgsty">
      <div class="caldata-possty type-js">
        <font class="text-js">{{ trans('index/language.splasql-data00040') }}</font>
        <div class="link-calsty623">
          <a data-toggle="modal" data-target="#contact_us_pp" class="cal-contactpopdata"><span class="cont-contpop"><img src="{{ url('images/contimg-sty.jpg') }}"></span><span class="cont-popcont65">{{ trans('index/language.splasql-data00041') }}</span></a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('after_scripts')
<script src="{{ asset('js/total.js') }}"></script>
<script src="{{ asset('js/calculadora.js') }}"></script>
<script>
  var count=2;
  var numeroMin=1;// 
  var numeroMax=19;//num = 20 host
  $(document).ready(function(){
  	$('[data-toggle="tooltip"]').tooltip({trigger: 'hover'}); 
  	$('#send').click(() => {
  		$('#formmail').submit();
  	});  
  });
  $(function(){
		$('.format').each((index, elem) => {
			$(elem).number( true, 2 );
		});
	});
</script>
@endsection