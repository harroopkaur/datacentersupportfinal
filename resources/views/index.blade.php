@extends('layouts.front')
@section('after_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
<style>
  .active-001{color: #63c6fb !important;}
</style>
@endsection
@section('content')
<input type="hidden" name="EstimadorAau" id="EstimadorAau" value="1">
<!-- ========================top hadding data start=================== -->
<div class="top-haddingsty951">
  <h4>{{ trans('index/language.home_data0001') }}</h4>
  <hr>
  <div class="clearfix"></div>
</div>
<!-- ====================top hadding data close======================== -->
<div class="row contitem">
  <form method="post" class="col-lg-6 mar-topsetting654">
    <div class="row">
        <div class="col-md-4 parale text-left">
          <label class="paralelogramo"><b>Enter your data</b></label>
        </div>
      </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label>{{ trans('index/language.home_data0002') }}</label>
      </div>
      <div class="col-lg-12">
        <input id="Vms" type="number" step="1" min="0" class="form-control fti" name="" placeholder="{{ trans('index/language.home_data0003') }}">
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label>{{ trans('index/language.home_data0004') }}</label>
      </div>
      <div class="col-lg-12">
        <input type="number" step="1" min="0" id="VMsWin" class="form-control fti" name="" placeholder="{{ trans('index/language.home_data0005') }}">
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label>{{ trans('index/language.home_data0006') }}</label>
      </div>
      <div class="col-lg-12">
        <input type="number" step="1" min="0" id="VMsApp" class="form-control fti" name="" placeholder="{{ trans('index/language.home_data0007') }}">
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label>{{ trans('index/language.home_data0008') }}</label>
      </div>
      <div class="col-lg-12">
        <input id="totalp" disabled="disabled" type="number" step="1" min="0" class="form-control fti bora" name="" placeholder="{{ trans('index/language.home_data0009') }}">
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label>{{ trans('index/language.home_data00010') }} </label>
      </div>
      <div class="col-lg-12">
        <input id="Vmsesca" type="number" step="1" min="0" class="form-control fti" name="" placeholder="{{ trans('index/language.home_data00011') }}">
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label>{{ trans('index/language.home_data00012') }}  </label>
      </div>
      <div class="col-lg-12">
        <input id="Vmsnoes" disabled="disabled" type="number" step="1" min="0" class="form-control fti bora" name=""  placeholder="{{ trans('index/language.home_data00013') }}" >
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label> {{ trans('index/language.home_data00014') }}  </label>
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-6">
            <div class="col-lg-12 lable-stydata951">
              <label> {{ trans('index/language.home_data00015') }} </label>
            </div>
            <input id="pEnt" type="number" step="1" min="0" name="" class="form-control fti" value="10" max="100">
          </div>
          <div class="col-lg-6">
            <div class="col-lg-12 lable-stydata951">
              <label> {{ trans('index/language.home_data00016') }} </label>
            </div>
            <input id="pStd" type="number" step="1" min="0" name="" class="form-control fti"  value="5" max="100">
          </div>
        </div>
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label> {{ trans('index/language.home_data00017') }}  </label>
      </div>
      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-6">
            <div class="col-lg-12 lable-stydata951">
              <label> {{ trans('index/language.home_data00018') }} </label>
            </div>
            <input id="tpEnt" type="number" step="1" min="0" name="" class="form-control fti" disabled="disabled" placeholder="{{ trans('index/language.home_data00019') }}">
          </div>
          <div class="col-lg-6">
            <div class="col-lg-12 lable-stydata951">
              <label> {{ trans('index/language.home_data00020') }} </label>
            </div>
            <input id="tpStd" type="number" step="1" min="0" name="" class="form-control fti" disabled="disabled" placeholder="{{ trans('index/language.home_data00021') }}">
          </div>
        </div>
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label>{{ trans('index/language.home_data00022') }} ($)</label>
      </div>
      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-6">
            <input id="costoEnt" type="text" min="0" name="" class="form-control fti format" placeholder="{{ trans('index/language.home_data00023') }}" value="600">
          </div>
          <div class="col-lg-6">
            <input id="costoStd" type="text" min="0" name="" class="form-control fti format" placeholder="{{ trans('index/language.home_data00024') }}" value="120">
          </div>
        </div>
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label>{{ trans('index/language.home_data00025') }} </label>
      </div>
      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-6">
            <input id="mrEnt" type="number" min="0" name="" class="form-control fti" placeholder="{{ trans('index/language.home_data00026') }}" value="36">
          </div>
          <div class="col-lg-6">
            <input id="mrStd" type="number" min="0" name="" class="form-control fti" placeholder="{{ trans('index/language.home_data00027') }}" value="36">
          </div>
        </div>
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label>{{ trans('index/language.home_data00028') }} ($)</label>
      </div>
      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-6">
            <input id="monEnd" type="text" min="0" name="" class="form-control bora fti format" readonly="readonly">
          </div>
          <div class="col-lg-6">
            <input id="monStd" type="text" min="0" name="" class="form-control bora fti format" data-toggle="tooltip" data-placement="Top" title="Monto para SQL Std" readonly="readonly">
          </div>
        </div>
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 calu-buttonsty951">
        <ul>
          <li>
            <button onclick="total()" type="button" class="btn btn-primary" style="border: 1px solid #f1f1f1;">{{ trans('index/language.home_data00029') }}</button>
          </li>
          <li>
            <button id="clear" class="btn btn-primary" type="reset">{{ trans('index/language.home_data00030') }}</button>
          </li>
        </ul>
      </div>
    </div>
    <div class="row ptt">
      <div class="col-lg-12 lable-stydata951">
        <label>{{ trans('index/language.home_data00031') }} ($) </label>
      </div>
      <div class="col-lg-12">
        <input id="cstp" type="text" min="0" name="" class="form-control fti format" placeholder="{{ trans('index/language.home_data00032') }}" disabled="disabled">
      </div>
    </div>
  </form>
  <div class="col-lg-6" style="background-color: #fff">
    <div class="home-currentaudit">
      <font class="risk-homeauditsty">
      {{ trans('index/language.home_data00033') }}
      $<span id="montss"> ______ </span> {{ trans('index/language.home_data00034') }} 
      </font>
    </div>
    <iframe class="homepage-videosty" 
      src="https://www.youtube.com/embed/OXZuqujL3nY">
    </iframe> 
    <br><br>
  </div>
</div>
@endsection
@section('after_scripts')
<script src="{{ asset('js/total.js') }}"></script>
<script>
  $(document).ready(function(){
  	$('[data-toggle="tooltip"]').tooltip({trigger: 'hover'}); 
  	$('#send').click(() => {
  		$('#formmail').submit();
  	});  
  });
  $(function(){
		$('.format').each((index, elem) => {
			$(elem).number( true, 2 );
		});
	});
</script>
@endsection