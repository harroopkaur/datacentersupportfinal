@extends('layouts.front')

@section('after_styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
	<style>
.active-003{color: #63c6fb !important;}
.active-sub001{color: #63c6fb !important;}
</style>
@endsection

@section('content')
<input type="hidden" name="calcSPLA" id="calcSPLA" value="1">
	  	<!-- 	<div class="row" >
	  			<div class="col-lg-12">
			  		<h1 class="col-lg-712 text-center">
						<b><font color="#002060">Calculadora</font></b>
						<b><font size="34em" color="#00b0f0" face="">SPLA</font></b>
				  	</h1>
				  	<div class="row">					  	
					  	<div class="col-lg-12 text-center">
							  <font color="#7f7f7f" size="2px">Determinar las licencias requeridas de Windows Server 2016 y los costos estimados</font>
							  <img src="{{asset('images/calc.png')}}" height="70" width="70"> <font color="#7f7f7f"><b>Resultados al instante</b></font>
					  	</div>
				  	</div>
	  			</div>
				</div> -->
				<div class="top-haddingsty951">
	<h4>{{ trans('index/language.calcul-data0001') }}</h4>
	<hr>
	<p>{{ trans('index/language.calcul-data0002') }} </p>
	<div class="clearfix"></div>
</div>
		
	  		<div class="row contitem">
	  			<form method="post" class="col-lg-6" style="padding-top: 0px; padding-bottom: 20px;padding-right: 35px;">

	  						<div class="row contitem">
					<div class="col-lg-12 lable-stydata951">
          <label>{{ trans('index/language.calcul-data0003') }}</label>
        </div>
					<div class="col-lg-12 mt-3">
						<select name="ediciones" id="ediciones" class="custom-select fti">
							<option value="1" selected>{{ trans('index/language.calcul-data0004') }}</option>
							<option value="2">{{ trans('index/language.calcul-data0005') }}</option>
						</select>
					</div>
					
				</div>

					<!-- 	<div class="row ptt">
							<div class="col-lg-4 text-left mb-4 pt-1 pb-1 alert alert-dark" role="alert" data-toggle="tooltip" data-placement="Top" title="Ingrese datos">
								<font size="2px" color="#002060">Ingrese Datos</font>
							</div>
							<div class="col-lg-8"></div>
						</div> -->
						<div class="row">
        <div class="col-lg-4 parale">
          <label class="paralelogramo"><b>{{ trans('index/language.calcul-data0006') }}</b></label>
        </div>
      </div>
						<div class="row ptt">
							
							<div class="col-lg-12 lable-stydata951">
          	<label>{{ trans('index/language.calcul-data0007') }}</label>
        		</div>
							<div class="col-lg-12 pr-0">
								<input id="procesadores" type="number" step="1" min="0" class="form-control fti" name="procesadores[]" placeholder="{{ trans('index/language.calcul-data0008') }}">
							</div>
						</div>
						<div class="row ptt">
							<div class="col-lg-12 lable-stydata951">
          	<label>{{ trans('index/language.calcul-data0009') }} </label>
        		</div>
							<div class="col-lg-12 pr-0">
								<input id="coresFisicos" type="number" step="1" min="0" class="form-control fti" name="coresFisicos[]" placeholder="{{ trans('index/language.calcul-data00010') }}">
							</div>
						</div>						
						<div id="host"></div>
	  				<div class="row ptt">
	  					<div class="col-lg-12 calu-buttonsty951">
	  						<ul>
	  							<li>	<button onclick="licenciamientoGeneral()" type="button" class="btn btn-primary" style="border: 1px solid #f1f1f1;">{{ trans('index/language.calcul-data00011') }}</button></li>
	  							<li><button id="clear" class="btn btn-primary" type="reset">{{ trans('index/language.calcul-data00012') }}</button>	</li>
	  						</ul>
							</div>
	  				
	  				
							<div class="col-lg-2 text-left">
							</div>
	  				</div>						
	  				<div class="row ptt">
	  					<div class="col-lg-12 lable-stydata951">
          	<label>{{ trans('index/language.calcul-data00013') }}</label>
        		</div>
	  					
	  					<div class="col-lg-12 pr-0">
	  						<input id="totalLicencia" type="number" min="0" name="totalLicencia" class="form-control fti format" placeholder="{{ trans('index/language.calcul-data00014') }}" data-toggle="tooltip" data-placement="Top" title="Cantidad de Licencias Requeridas" readonly="readonly">
	  					</div>
						</div>
						<div class="row ptt">
							<div class="col-lg-12 calu-buttonsty951">
								<ul>
									<li><button onclick="addHost()" class="btn btn-primary" type="button">{{ trans('index/language.calcul-data00015') }}
</button>	</li>
								<button id="rHost" onclick="removeHost()" class="btn btn-warning" type="button" disabled> {{ trans('index/language.calcul-data00016') }}
</button>										
									<li></li>
								</ul>
							</div>
						
						
						</div>
						<div class="row ptt mt-3">
							<div class="col-lg-4 parale"><label class="paralelogramo"><b>{{ trans('index/language.calcul-data00017') }}</b></label></div>
						
						</div>
						<div class="row ptt">
								<div class="col-lg-12 lable-stydata951">
          	<label>{{ trans('index/language.calcul-data00018') }}</label>
        		</div>
        		<div class="col-lg-12 lable-stydata951">
          	<label style="padding-left: 20px;">{{ trans('index/language.calcul-data00019') }}</label>
        		</div>
						
						
						</div>
						<div class="row ptt">
								<div class="col-lg-12 lable-stydata951">
          	<label>{{ trans('index/language.calcul-data00020') }}</label>
        		</div>
							<div class="col-lg-12 pr-0">
								<input type="number" step="1" min="0" id="precioUnitario" class="form-control fti" name="precioUnitario" value="0" disabled>
							</div>
						</div>
						<div class="row ptt">
							<div class="col-lg-12 lable-stydata951">
          	<label>{{ trans('index/language.calcul-data00021') }}</label>
        		</div>
								<div class="col-lg-12 pr-0">
									<input type="number" step="1" min="0" id="totalLicenciaRequerida" class="form-control fti" name="totalLicenciaRequerida" value="0" disabled>
								</div>
						</div>
						
							<div class="row ppt">
								<div class="col-lg-12 get-youresult951 pr-0">
									<a href="#" data-toggle="modal" data-target="#rcv_audit_dfnce">{{ trans('index/language.calcul-data00022') }}</a>
								</div>
							</div>
	  			</form>

	  			<div class="col-lg-6">
					 
	  				
	  			<div class="cal-databgsty">
	  				<div class="caldata-possty  type-js">
	  					<font class="text-js">
	  						{{ trans('index/language.calcul-data00023') }}
	  					</font>
	  					<div class="link-calsty623">
	  						<a  data-toggle="modal" data-target="#contact_us_pp" class="cal-contactpopdata"><span class="cont-contpop"><img src="{{ url('images/contimg-sty.jpg') }}"></span><span class="cont-popcont65">{{ trans('index/language.calcul-data00024') }}</span></a>
	  					</div>
	  				</div>
	  				</div>
	  				
	  			
	  			</div>
	  		</div>
@endsection

@section('after_scripts')
	<script src="{{ asset('js/total.js') }}"></script>
	<script src="{{ asset('js/calculadora.js') }}"></script>
	<script>
		var count=2;
		
		var numeroMin=1;// 
		var numeroMax=19;//num = 20 host
		$(document).ready(function(){
		  	$('[data-toggle="tooltip"]').tooltip({trigger: 'hover'}); 

		  	$('#send').click(() => {
		  		$('#formmail').submit();
		  	});  
		});
		$(function(){
				$('.format').each((index, elem) => {
					$(elem).number( true, 2 );
				});
			});
	</script>
	<script>
		function addHost(){
    if (numeroMin<=numeroMax) {
    $("#host").append('<div id="bloque'+count+'"><div class="row ptt">'+
    '<div class="col-lg-4 parale">'+
            '<label class="paralelogramo"><b>{{ trans('index/language.calcul-data00025') }} '+count+'</b></label>'+
    '</div>'+
    '<div class="col-lg-12 lable-stydata951">'+
        '<label size="1px" color="#002060"> {{ trans('index/language.calcul-data0007') }}</label>'+
    '</div>'+
    '<div class="col-lg-12 pr-0">'+
        '<input id="procesadores" type="number" step="1" min="0" class="form-control fti" name="procesadores[]" placeholder="Ingrese el total de VMs">'+
    '</div>'+
    '</div>'+
    '<div class="row ptt">'+
        '<div class="col-lg-12 lable-stydata951">'+
            '<label size="1px" color="#002060"> {{ trans('index/language.calcul-data0009') }} </label>'+
        '</div>'+
        '<div class="col-lg-12 pr-0">'+
            '<input type="number" step="1" min="0" id="coresFisicos" class="form-control fti" name="coresFisicos[]" placeholder="Total VMs Escaneadas">'+
        '</div>'+
    '</div></div>');
    count++;
    numeroMin++;
    $('#rHost').attr("disabled", false);   
    }
}
	</script>
@endsection