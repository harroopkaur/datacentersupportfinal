<!DOCTYPE html>
<html lang="es">
<head>
    <style>
        .button {
            background-color: #2980b9; /* Green */
            border: none;
            border-radius: 25px;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin-left: 2%;
        }
        .footer{
            bottom: 0;
            position: fixed !important;
        }
    </style>
    <meta charset="UTF-8">
    <meta name="viewport" title="Correo de confirmacion de Usuario" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Bienvenida a nuevo usuario</title>
</head>
<body>
    <div class="col-lg-12 col-xs-12 container" style="width: 100%;">
        <img src="{{ url('images/logo-header.png') }}" style="width: 100%;">
        <div style="text-align: center;">
            <img src="{{ url('images/header.png') }}">
        </div>
        <div style="padding: 0 100px 0 100px;">
            <p>Estimado {{$name}},</p>
            <p>Agradecemos su interes,</p>
            <p>A través de este correo podrá descargar un PDF con valiosos TIPS acerca de las mejores prácticas de getión de auditorías de software.</p>
            <p>Si Ud. Se encuentra enfrentando un proceso de auditoría, no dude en solicitar soporte personalizado, gratuito y confidencial a través de nuestras vías de contacto: +1 (305)851-3545 o mediante correo electrónico a <a href="mailto:info@licensingassurance.com">info@licensingassurance.com</a></p>
        </div>
        <img src="{{ url('images/logo-footer.png') }}" style="width: 100%;">
    </div>
</body>
</html>